#ifndef EVENT_H
#define EVENT_H

#include <sys/uio.h> // for iovec
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <cstdint>
#include <memory>
#include <ostream>
#include <vector>
#include "Tag.h"
#include "Types.h"
#include "eformat/StreamTag.h"
#include "ers/Issue.h"
namespace SFOng { class Input; }
namespace SFOng { class StatsCollector; }

ERS_DECLARE_ISSUE(SFOng,
    InvalidEvent,
    ERS_EMPTY,
    ((std::uint64_t) gid))

ERS_DECLARE_ISSUE_BASE(SFOng,
    MalformedEvent,
    InvalidEvent,
    "Malformed event: " << reason << " (GID: " << gid << ")",
    ((std::uint64_t) gid),
    ((const char* ) reason))

ERS_DECLARE_ISSUE_BASE(SFOng,
    InconsistentGIDs,
    InvalidEvent,
    "Multiple event versions with different GIDs (" << gid << " and " << gid2 << ")",
    ((std::uint64_t) gid),
    ((std::uint64_t) gid2))

ERS_DECLARE_ISSUE_BASE(SFOng,
    InconsistentRunNumber,
    InvalidEvent,
    "The event run number (" << event_runno
    << ") does not match the actual run number (" << runno << ")",
    ((std::uint64_t) event_runno),
    ((std::uint64_t) runno))

ERS_DECLARE_ISSUE_BASE(SFOng,
    InconsistentLBs,
    InvalidEvent,
    "Multiple event versions with different Luminosity Block Numbers " <<
    "(GID: " << gid << "; LBs: " << lb1 << " and " << lb2 << ")",
    ((std::uint64_t) gid),
    ((uint16_t) lb1) ((uint16_t) lb2))

ERS_DECLARE_ISSUE_BASE(SFOng,
    InconsistentStreamTags,
    InvalidEvent,
    "Multiple event versions with different Stream Tags (GID: " << gid << ")",
    ((std::uint64_t) gid),
    ERS_EMPTY)

namespace SFOng
{

struct EventStream
{
  Tag tag;
  IoVector iovector;
  uint32_t uncompressedDataSize; // Total amount of data when not compressed: includes data and headers

#ifndef ERS_NO_DEBUG
  std::string debugStr() const;
#endif // !defined(ERS_NO_DEBUG)
};

class Event
{

public:

  Event(StatsCollector& sc, Input& i,
        std::uint64_t gid, std::uint32_t run_number,
        BufferHandle b, bool lumiblockEnabled);

  Event(StatsCollector& sc, Input& i, std::uint64_t gid, BufferHandle b, const Tag& singleTag);

  ~Event();

  Event(Event const &) = delete;
  Event& operator=(Event const &) = delete;

  std::uint64_t gid() const
  {
    return m_gid;
  }

  std::uint16_t lumiblock() const
  {
    return m_lb;
  }

  const std::vector<EventStream>& rawStreams() const
  {
    return m_rawStreams;
  }

  const std::vector<EventStream>& fileStreams() const
  {
    return m_fileStreams;
  }

  std::uint32_t rawSize() const
  {
    return m_rawSize;
  }

  std::uint32_t fullEventUncompressedSize() const
  {
    return m_fullEventUncompressedSize;
  }

  void compress(uint16_t compressionLevel);

  void toSingleFileStream(const Tag& tag);

#ifndef ERS_NO_DEBUG
  std::string debugStr(bool withStreams = false) const;
#endif // !defined(ERS_NO_DEBUG)

private:

  struct View
  {
    std::vector<eformat::helper::StreamTag> streamTags;
    iovec iov;
    // Total amount of data when not compressed: includes data and headers
    uint32_t uncompressedDataSize;
  };

  Event(StatsCollector& sc, Input& i, std::uint64_t gid, BufferHandle b);

  Input& m_input;
  StatsCollector& m_statsCollector;

  boost::posix_time::ptime m_creationTimestamp;

  BufferHandle m_buffer;

  std::uint64_t m_gid;
  std::uint16_t m_lb;
  std::uint32_t m_rawSize; // sum of the size of all the different fragments
  std::uint32_t m_fullEventUncompressedSize; // Total amount of data when not compressed: includes data and headers

  std::vector<View> m_views;
  std::vector<EventStream> m_rawStreams;
  std::vector<EventStream> m_fileStreams;
  std::unique_ptr<std::uint32_t[]> m_singleFragment;

};

} // namespace SFOng

#endif // EVENT_H
