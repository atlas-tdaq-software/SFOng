#include "WritingTask.h"
#include <sys/uio.h> // iovec
#include "Event.h"
#include "EventStorage/DataWriter.h"
#include "StatsCollector.h"
#include "WriterSerializer.h"
#include "ers/ers.h"
#include "src/Tag.h"
#include "src/Types.h"

namespace SFOng
{

namespace pt = boost::posix_time;

WritingTask::WritingTask(
    StatsCollector& sc,
    const WriterSerializerHandle& ws,
    const EventHandle& event,
    const EventStream& stream):
  m_statsCollector(sc),
  m_writer(ws),
  m_event(event),
  m_stream(stream),
  m_creationTimestamp(pt::microsec_clock::universal_time())
{
  m_statsCollector.onWtCreation(m_creationTimestamp, m_stream);
  ERS_DEBUG(3, "WritingTask: created for event " << m_event->debugStr() << " stream " << m_stream.debugStr());
}

void WritingTask::operator()()
{
  pt::ptime const begin_ts = pt::microsec_clock::universal_time();

  ERS_DEBUG(3, "WritingTask: writing event " << m_event->debugStr() << " stream " << m_stream.debugStr());

  m_writer->ESWriter()->putData(m_stream.iovector.size(), m_stream.iovector.data());

  bool success = true;
  if (m_writer->ESWriter()->good()) {
    ERS_DEBUG(3, "WritingTask: written event " << m_event->debugStr() << " stream " << m_stream.debugStr());
  } else {
    success = false;
    ers::error(WritingTaskIssue(ERS_HERE, m_event->gid(), m_stream.tag.fullName()));
    // try to advance to the the next file, maybe we are lucky
    m_writer->ESWriter()->closeFile();
  }

  auto const now = pt::microsec_clock::universal_time();
  m_statsCollector.onWtExecution(now, m_stream, now - begin_ts, success);

  /* We can't call onWtDestruction from WritingTask's destructor because
   * WritingTasks are created on the stack in WritingManager::addEVent(..)
   * (the Asio strand makes a copy of it), so they are destroyed very quickly
   * and it does not represent the lifetime of the actual writing task.
   * But there should not be much happenning between here and the destructor's
   * call, so this way of measuring the life time should be close to accurate.
   */
  m_statsCollector.onWtDestruction(now, m_stream, now - m_creationTimestamp);

  ERS_DEBUG(3, "WritingTask: destroyed for event " << m_event->debugStr() << " stream " << m_stream.debugStr());
}

uint64_t WritingTask::weight() const
{
  uint64_t rv = 0;
  for (iovec const & iov: m_stream.iovector)
  {
    rv += iov.iov_len;
  }
  return rv;
}

boost::posix_time::ptime WritingTask::timestamp() const
{
  return m_creationTimestamp;
}

} // namespace SFOng
