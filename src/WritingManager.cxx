#include "WritingManager.h"
#include <exception>
#include <algorithm>
#include <boost/algorithm/string/join.hpp>
#include <boost/lexical_cast.hpp>
#include <functional>
#include <set>
#include <stdexcept>
#include <unordered_map>
#include "Config.h"
#include "Event.h"
#include "EventStorage/DataWriter.h"
#include "RunControl/Common/RunControlCommands.h"
#include "SFOTZ/DBUpdateInfo.h"
#include "SfotzPublisher.h"
#include "WriterSerializer.h"
#include "WritingTask.h"
#include "ers/ers.h"
#include "src/DirectoryManager.h"
#include "src/Tag.h"
#include "src/Types.h"
#include "src/WeightedIOService.h"

namespace fs = std::filesystem;
namespace pt = boost::posix_time;

namespace SFOng
{

WritingManager::WritingManager(const Config& config, SfotzPublisher& sfotzPublisher, StatsCollector& sc)
  : daq::rc::Controllable()
  , m_config(config)
  , m_isXoff()
  , m_lumiblockLimit()
  , m_maxLumiblockLifetime()
  , m_lbCloseMethodFallback(false)
  , m_maxOpenLumiblock(10)
  , m_statsCollector(sc)
  , m_sfotzPublisher(sfotzPublisher)
  , m_lockFileName()
  , m_trackerRunning()
  , m_trackerThread()
  , m_rotateNow()
  , m_streamWeightPeriod()
{
}

void WritingManager::configure(const daq::rc::TransitionCmd&)
{
  if (!m_config.writingEnabled()) {
    return;
  }

  int const concurrency_hint = m_config.nWorkerThreads();
  boost::posix_time::time_duration const io_service_weight_period =
      boost::posix_time::seconds(m_config.ioServiceWeightPeriod());

  for (std::uint32_t i = 0; i < m_config.nWorkerThreads(); ++i) {
    // Create io_service
    m_ioServices.emplace_back(
        concurrency_hint,
        io_service_weight_period,
        m_config.assignPolicyWatchedStreams());

    auto& ioService = m_ioServices.back().m_ioService;

    // Prevent io_service::run() from returning
    // when there are no pending asynchronous operations
    m_ioServiceWorks.emplace_back(ioService);

    // Run io_service
    m_ioServiceThreads.emplace_back([&] () {
        ioService.run();
      });
  }

  m_nextIoService = m_ioServices.begin();

  m_lockFileName = m_config.applicationName() + ".lock";

  m_streamWeightPeriod = boost::posix_time::seconds(m_config.streamWeightPeriod());

  m_maxLumiblockLifetime = m_config.maxLumiblockLifetime();
  ERS_LOG("MaxLumiblockLifetime (s): " << m_maxLumiblockLifetime);

  //We perform an early check of the paths.
  //This is important because it allows reporting problems
  //before the start of run
  preparePaths();
}

void WritingManager::prepareForRun(const daq::rc::TransitionCmd&)
{
  if (!m_config.writingEnabled()) {
    return;
  }

  //This is needed to reset the path list. In fact paths
  //were possibly added/removed during the previous run
  //with user commands
  preparePaths();

  try {
    m_path = getNextPath();
  } catch (WritingManagerNoUsablePath& e) {
    ers::error(e);
    m_isXoff = true;
  }

  try {
    lockPath(m_path);
  } catch (WritingPathIssue& e) {
    ers::error(WritingManagerPathInfo(ERS_HERE, e));
  }

  if (m_config.unlockDirDbSync()) {
    ERS_LOG("instantiating DirectoryManager to synchronize directory unlock with DB");
    m_dir_manager = std::make_unique<DirectoryManager>();
    std::set<SFOTZ::DBUpdateInfo::CommandType> sfotz_command_types;
    sfotz_command_types.insert(SFOTZ::DBUpdateInfo::FILECLOSED);
    ERS_DEBUG(0, "WritingManager::prepareForRun: registering dir_manager to SFOTZ main thread");
    if (!m_sfotzPublisher.registerSFOTZCallbackToMainDbThread(m_dir_manager.get(), sfotz_command_types)) {
      ERS_LOG("could not register DirectoryManager to main DB thread: disabling DirectoryManager");
      m_dir_manager.reset();
    }
  }

  // Fetch information on stale LBs (from a previously existing instance)
  std::unordered_map<Tag, unsigned int> staleTags;
  unsigned int maxLumiblock = m_sfotzPublisher.staleTags(staleTags);
  static_cast<void>(maxLumiblock); // unused value

  // Store the start index per stale tag
  // We will use this later to either
  // a) initialise writers at the correct file index
  // b) close stale LB in the databases
  m_indexmap.insert(staleTags.begin(), staleTags.end());

  // Move stale files
  m_sfotzPublisher.moveStaleFiles();

  // start tracker to periodically close old lumiblocks and trigger path changes
  ERS_DEBUG(2, "WritingManager: tracker starting");
  m_trackerRunning = true;
  m_trackerThread = new std::thread(&WritingManager::tracker, this);
}

void WritingManager::stopRecording(const daq::rc::TransitionCmd&)
{
  if (!m_config.writingEnabled()) {
    return;
  }

  {
    std::lock_guard<std::mutex> lock(m_trackerRunningMutex);
    m_trackerRunning = false;
  }
  m_trackerRunningCond.notify_all();
  m_trackerThread->join();
  ERS_DEBUG(2, "WritingManager: tracker stopped");
  delete m_trackerThread;

  // destroy all writers
  {
    tbb::queuing_rw_mutex::scoped_lock lock(m_mapsMutex, true);

    // Close the left over stale LB (it should be only LB=0 at this
    // stage
    for (const auto& it: m_indexmap) {
      m_sfotzPublisher.publishLumiblockClosed(it.first);
    }
    m_indexmap.clear();
    m_map.clear();
  }

  try {
    unlockPath(m_path);
    if (!m_otherPath.empty()) { // we were stopped in the middle of a path transition
      unlockPath(m_otherPath);
    }
  } catch (WritingPathIssue& e) {
    ers::error(WritingManagerPathInfo(ERS_HERE, e));
  }

  // reset variables
  m_isXoff = false;
  m_lumiblockLimit = 0;
  m_lumiblockTimes.clear();
  m_path.clear();
  m_otherPath.clear();

  // Here it's safe to call clear() on the TBB queue: elements can still be pushed
  // to it via user() but user() and run control transitions are serialized.
  // We are no longer in a concurrent context.
  m_commandQueue.clear();
}

void WritingManager::unconfigure(const daq::rc::TransitionCmd&)
{

  // Allow io_service::run() to return when there are no pending asynchronous operations
  m_ioServiceWorks.clear();
  // Wait for io_service::run() to return (i.e. wait for all asynchronous operations to finish)
  for (auto& thread : m_ioServiceThreads) {
    thread.join();
  }
  m_ioServiceThreads.clear();
  // Destroy io_services.
  m_ioServices.clear();

}

void WritingManager::user(const daq::rc::UserCmd& cmd)
{
  m_commandQueue.try_push(cmd.arguments());
  //Wake up the tracker
  m_trackerRunningCond.notify_all();
}

void WritingManager::addEvent(EventHandle event)
{
  ERS_ASSERT(m_config.writingEnabled());

  /* We have to guarantee lumiblocks won't be closed while
   * we are assigning streams to the corresponding writing tasks
   *
   * If you don't lock here, the event can be considered non-late, then the
   * associated writer is closed and m_lumiblockLimit is updated accordingly,
   * then the writer is *wrongly* (re)created.
   */
  tbb::queuing_rw_mutex::scoped_lock lock(m_lumiblockMutex, false);

  if (checkLate(event->lumiblock())) {
    event->toSingleFileStream(m_config.lateEventsTag());
  } else {
    // Late events must not recreate an entry in m_lumiblockTimes

    // The following access to m_lumiblockTimes is under the guard of m_lumiblockMutex
    if (event->lumiblock() != 0 && (m_lumiblockTimes.empty() || event->lumiblock() > m_lumiblockTimes.back().first)) {
      /* First event for a new lumiblock: create the entry in the lumiblock times
      * queue. Write-locking "lock" blocks every incoming events: it must be quick, and it
      * should not happen too frequently (once every minute in normal conditions).
      */
      lock.upgrade_to_writer();
      /* Several threads could reach this point (m_lumiblockMutex read locks) with
      * the same event_lb so, now that the mutex is write-locked, we must test
      * the condition again to avoid adding the same entry multiple times.
      */
      if (m_lumiblockTimes.empty() || event->lumiblock() > m_lumiblockTimes.back().first) {
        m_lumiblockTimes.push_back(std::make_pair(event->lumiblock(), std::chrono::steady_clock::now()));
      }
      lock.downgrade_to_reader();
    }
  }

  // enqueue writing tasks
  for (const auto& stream : event->fileStreams()) {
    WriterSerializerHandle ws = this->getWriter(stream.tag);
    ws->post(WritingTask(m_statsCollector, ws, event, stream));
  }
}

bool WritingManager::checkLate(uint16_t lumiblock)
{
  // An event can be late only if
  // - per-lumiblock streaming is enabled
  // - m_lumiblockLimit is non-zero (since zero is the default value)
  // - the lumiblock is non-zero (corrupted events have zero)
  return m_config.lumiblockEnabled() && m_lumiblockLimit && lumiblock && (lumiblock <= m_lumiblockLimit);
}

WriterSerializerHandle WritingManager::getWriter(const Tag& tag)
{
  ERS_DEBUG(4, "WritingManager: getting writer " << tag.fullName() << " LB " << tag.lumiblock);

  /* WARNING
     This method is executed in parallel by many threads. Indeed
     the following mutex allows concurrent access for multiple "readers".
     The locking for the writer maps is achieved using the TBB accessors.
     On the other hand, any other data structure needs to be explicitly
     protected
   */

  tbb::queuing_rw_mutex::scoped_lock lock(m_mapsMutex, false);

  Map::const_accessor ca;
  if (m_map.find(ca, tag)) {
    ERS_DEBUG(3, "WritingManager: using writer " << tag.fullName() << " LB " << tag.lumiblock);
    return ca->second;
  } else {
    Map::accessor a;
    if (m_map.insert(a, tag)) {
      try {
        /* We can't afford to die until the WriterSerializer is actually
         * created now. Because if for example an exception is raised, the
         * thread will die, the accessor will be destroyed and so the lock
         * will be released. Other threads will access the inserted value
         * whereas it is still invalid (most likely null pointer).
         *
         * The result of such a thing happening will most likely be a quick
         * segfault when the other threads use the uninitialised pointer.
         */

        ERS_DEBUG(3, "WritingManager: creating writer " << tag.fullName() << " LB " << tag.lumiblock);

        // Check in the indexmap at which index the new writer
        // should start
        unsigned int index = 0;
        IndexMap::const_accessor id;
        if (m_indexmap.find(id, tag)) {
          index = id->second;
          m_indexmap.erase(id);
        }

        // Retrieve or create the corresponding stream's Weight
        StreamWeightMap::accessor swa;
        if (m_streamWeightMap.insert(swa, tag)) {
          // the SlidingWindowStats that serves the weight for this stream
          // has just been created (and inserted): set period
          swa->second.setPeriod(m_streamWeightPeriod);
        }

        std::list<WeightedIOService>::iterator io_service;

        {
          // This scope needs to be locked, since we access containers
          // potentially from multiple threads
          // See warning at the beginning of the method.
          std::unique_lock<std::mutex> hardlock(m_getWriterMutex);
          io_service = m_nextIoService;

          // Select the correct I/O service for this writer.
          if (m_config.ioServiceAssignPolicy() == AssignPolicy::OccupancyWeighted) {
            // Here we make use of the weight to select the least-used service
            // (the smallest weight)
            io_service = std::min_element(m_ioServices.begin(), m_ioServices.end(),
                [](const WeightedIOService& w1, const WeightedIOService& w2){
                    return w1.weight() < w2.weight();});
          }
          else if (m_config.ioServiceAssignPolicy() == AssignPolicy::DebugAssignErrors) {
            if ((tag.type == "physics" && tag.name == "bulk") ||
                (tag.type == "physics" && tag.name == "delay1") ||
                (tag.type == "physics" && tag.name == "delay2")
                ) {
              io_service = m_ioServices.begin();
            }

            if (++m_nextIoService == m_ioServices.end()) {
              m_nextIoService = m_ioServices.begin();
            }
          } else {
            // By default we use a round robin policy
            if (++m_nextIoService == m_ioServices.end()) {
              m_nextIoService = m_ioServices.begin();
            }
          }

          /* As this stream will be handled by this thread, we want the
           * thread's weight to reflect this immediately, so that it is taken
           * into account when selecting threads. This is why we have to do it
           * while holding the "hardlock": so that no decision is taken without
           * taking into account the newly added weight.
           * This way, a thread starting to handle a heavy stream will less
           * likely be selected for other streams IMMEDIATELY. Which is what we
           * want.
           * Also note that if by chance a thread gets allocated two (or more)
           * LB of the same stream, each LB will add the weight of the whole
           * stream (not only the corresponding LB's) to the thread's weight
           * again and again.
           */
          io_service->addWeight(boost::posix_time::microsec_clock::universal_time(), swa->second.sum());
        }

        a->second.reset(new WriterSerializer(m_config, *io_service,
            m_sfotzPublisher, tag, m_path, index, swa->second, m_dir_manager.get()));
      }
      catch (std::exception const & e) {
        ERS_LOG("WritingManager: exception while creating WriterSerializer: " << e.what());
        m_map.erase(a);
        return WriterSerializerHandle();
      }
      catch (...) {
        ERS_LOG("WritingManager: unknown exception while creating WriterSerializer");
        m_map.erase(a);
        return WriterSerializerHandle();
      }
    } else {
      // 'insert' found an element and updated the accessor
      // nothing to do here
      ERS_DEBUG(3, "WritingManager: using writer " << tag.fullName() << " LB " << tag.lumiblock);
    }

    return a->second;
  }
}

float WritingManager::getAvailableFraction(const std::filesystem::path& path)
{
  float availFrac;
  try {
    fs::space_info si = fs::space(path);
    availFrac = ((float) si.available) / si.capacity;
  } catch (fs::filesystem_error& e) {
    throw WritingPathFreeSpaceError(ERS_HERE, path, e);
  }
  if (availFrac > 1.) {
    throw WritingPathFreeSpaceError(ERS_HERE, path);
  }
  return availFrac;
}

void WritingManager::lockPath(const std::filesystem::path& path)
{
  try {
    fs::path lockFilePath = path / m_lockFileName;
    std::ofstream lockFile;
    lockFile.exceptions(std::ofstream::failbit | std::ofstream::badbit);
    lockFile.open(lockFilePath);
    lockFile << "SFO lock file";
  } catch (std::ofstream::failure& e) {
    throw WritingPathCannotLock(ERS_HERE, path, e);
  }
}

void WritingManager::unlockPath(const std::filesystem::path& path)
{
  try {
    static unsigned const UNLOCK_PATH_TIMEOUT_MS = m_config.actionTimeout().total_milliseconds() / 2;
    if (m_dir_manager) {
      bool rv = m_dir_manager->wait_for_empty_dir(path.string(), UNLOCK_PATH_TIMEOUT_MS);
      if (!rv) {
        // timeout
        ers::warning(UnlockedWritingPathUnsynced(ERS_HERE, path));
        m_dir_manager->clear_dir_entries(path.string());
      }
    }
    fs::path lockFilePath = path / m_lockFileName;
    fs::remove(lockFilePath);
  } catch (std::runtime_error& e) {
    throw WritingPathCannotUnlock(ERS_HERE, path, e);
  }
}

void WritingManager::closeOldLumiblocks()
{
  std::vector<WriterSerializerHandle> files_to_close;

#ifndef ERS_NO_DEBUG
  auto const start_ts = std::chrono::high_resolution_clock::now();
  std::chrono::nanoseconds lock_dur(0), max_lock_dur(0), queue_lock_dur(0);
  std::chrono::time_point<std::chrono::high_resolution_clock> lock_start_ts, lock_end_ts;
  std::size_t map_size, queue_size;
  uint16_t lumiblocklimit_copy;
#endif

  // Scope for m_lumiblockMutex lock
  {
    // Block incoming events processing
    tbb::queuing_rw_mutex::scoped_lock lblock(m_lumiblockMutex, true);
#ifndef ERS_NO_DEBUG
    lock_start_ts = std::chrono::high_resolution_clock::now();
    queue_size = m_lumiblockTimes.size();
#endif

    if (m_lbCloseMethodFallback) {
      /* Operations Safety: with this method m_lumiblockLimit is determined,
       * as it used to, by: max_received_lb - max_open_lb
       */
      if (m_lumiblockTimes.empty()) {
        return;
      }
      uint16_t const max_lb = m_lumiblockTimes.back().first;
      if (max_lb <= m_maxOpenLumiblock) {
        return;
      }
      m_lumiblockLimit = max_lb - m_maxOpenLumiblock;

      // m_lumiblockTimes still need to be cleared accordingly
      for (TLumiblockTimes::iterator it(m_lumiblockTimes.begin()); it != m_lumiblockTimes.end(); ++it) {
        if (it->first <= m_lumiblockLimit) {
          it = m_lumiblockTimes.erase(it);
        } else {
          // Entries in m_lumiblockTimes are strictly increasing, so once
          // we found one greater than m_lumiblockLimit, we're done.
          break;
        }
      }
    } else {
      auto const now = std::chrono::steady_clock::now();
      TLumiblockTimes::iterator it(m_lumiblockTimes.begin()), end(m_lumiblockTimes.end());
      for (; it != end; ++it) {
        uint32_t const lb_lifetime_s = std::chrono::duration_cast<std::chrono::seconds>(
            now - it->second).count();
        if (lb_lifetime_s > m_maxLumiblockLifetime) {
          /* Note that m_lumiblockLimit is strictly increasing because new LBs
           * are inserted if they are greater than m_lumiblockLimit.back().
           *
           * If LBs arrive out of order, e.g. [1, 3, 2], LB 2 will be closed
           * when 3 is closed.
           *
           * This situation is very unlikely but possible if: LBs are short,
           * processing time for LB 3 is short (at least for the first event),
           * processing time for LB 2 is long enough.
           *
           * Another note: when LB N has lived more than the
           * m_maxLumiblockLifetime, event from LB *N-1* are late. Because we
           * assume that first event of LB N follows last event of LB N-1,
           * which is not necessarily true. The config parameter
           * MaxLumiblockLifetime *MUST* contain the necessary margin to make
           * it true.
           */
          m_lumiblockLimit = it->first;
          // Clear LBs that are going to be closed from the queue
          it = m_lumiblockTimes.erase(it);
        } else {
          // As soon as we found one, the following ones will necessarily have a
          // later creation because they are inserted in time order.
          break;
        }
      }
    }

#ifndef ERS_NO_DEBUG
    lumiblocklimit_copy = m_lumiblockLimit; // copy for debug log
    lock_end_ts = std::chrono::high_resolution_clock::now();
    auto const ldur = lock_end_ts - lock_start_ts;
    queue_lock_dur = ldur;
    lock_dur += ldur;
    if (ldur > max_lock_dur) max_lock_dur = lock_dur;
#endif
  }
  ERS_DEBUG(0, "lumiblock limit: " << m_lumiblockLimit);
  /* Unlock m_lumiblockMutex: incoming events can be checked against late-ness
   * again, even if the corresponding writers are not closed yet. They cannot
   * be assigned to these writers anyway because they will be declared late
   * and assigned to the late-events writer.
   *
   * This "gap" between the two locks allows for the processing of incoming
   * events and release "pressure" on these locks.
   */

  // Scope for m_mapsMutex lock
  {
    tbb::queuing_rw_mutex::scoped_lock maplock(m_mapsMutex, true);
#ifndef ERS_NO_DEBUG
    lock_start_ts = std::chrono::high_resolution_clock::now();
    map_size = m_map.size();
#endif

    // If the indexmap still contains entries for old lumiblocks, then we
    // have to manually close the DB entry here
    // LB = 0 has to be skipped, since those events can come at any time
    // in future
    {
      IndexMap::const_iterator id_it = m_indexmap.begin();
      while (id_it != m_indexmap.end()) {
        if (id_it->first.lumiblock != 0 && id_it->first.lumiblock <= m_lumiblockLimit) {
          m_sfotzPublisher.publishLumiblockClosed(id_it->first);
          m_indexmap.erase(id_it->first);
          // all iterators are now invalid
          // we can only restart from the beginning
          id_it = m_indexmap.begin();
        } else {
          id_it++;
        }
      }
    }

    {
      Map::const_iterator ci = m_map.begin();
      while (ci != m_map.end()) {
        if (ci->first.lumiblock != 0 && ci->first.lumiblock <= m_lumiblockLimit) {
          files_to_close.push_back(ci->second);

          m_map.erase(ci->first);

          /* This operation (closeOldLumiblocks) could be long: with a map
           * containing several thousands of elements, we measured that it can
           * take up to 60 ms, while in normal conditions the map contains only
           * a few tens elements and the usual duration is ~160 us.
           * While the m_mapsMutex is locked here, incoming events cannot be
           * dispatched; so in extreme situations, input queues could fill up,
           * eventually leading to backpressure or even DCM disconnections.
           *
           * As we have to restart from the beginning of the map, it is a good
           * moment to release the lock for a short while. And it is safe to
           * release the lock at this point. This will allow some incoming
           * events to be processed.
           */
#ifndef ERS_NO_DEBUG
          lock_end_ts = std::chrono::high_resolution_clock::now();
          auto const ldur = lock_end_ts - lock_start_ts;
          lock_dur += ldur;
          if (ldur > max_lock_dur) max_lock_dur = lock_dur;
#endif
          maplock.release(); // RELEASE LOCK

          // Make sure the OS scheduler has the opportunity to schedule the
          // input threads
          std::this_thread::sleep_for(std::chrono::milliseconds(50));

          maplock.acquire(m_mapsMutex, true); // ACQUIRE LOCK
#ifndef ERS_NO_DEBUG
          lock_start_ts = std::chrono::high_resolution_clock::now();
#endif

          // all iterators into m_map are now invalid
          // we can only restart from the beginning
          ci = m_map.begin();
        } else {
          ci++;
        }
      }
    }

#ifndef ERS_NO_DEBUG
    lock_end_ts = std::chrono::high_resolution_clock::now();
    auto const ldur = lock_end_ts - lock_start_ts;
    lock_dur += ldur;
    if (ldur > max_lock_dur) max_lock_dur = lock_dur;
#endif
  } // RELEASE LOCKS: do not manipulate maps anymore

#ifndef ERS_NO_DEBUG
  auto const before_file_close_ts = std::chrono::high_resolution_clock::now();
#endif

  /* Now that locks are released, we can close files which can be a long
   * operation. Measurements in SFOTest: 5 to 15 ms by file close on average.
   */
#ifndef ERS_NO_DEBUG
  auto const nb_closed_files = files_to_close.size();
#endif
  files_to_close.clear();

#ifndef ERS_NO_DEBUG
  auto const end_ts = std::chrono::high_resolution_clock::now();
#endif

  ERS_DEBUG(0, "closeOldLumiblocks:"
      << " method = " << (m_lbCloseMethodFallback ? "lb-based" : "time-based")
      << " lb_limit = " << lumiblocklimit_copy
      << " map_size = " << map_size
      << " q_size = " << queue_size
      << " q_lock_duration = " << queue_lock_dur.count() / 1000 << " us"
      << " total_lock_duration = " << lock_dur.count() / 1000 << " us"
      << " max_lock_duration = " << max_lock_dur.count() / 1000 << " us"
      << " total_duration = " << (end_ts - start_ts).count() / 1000 << " us"
      << " nb_closed_files = " << nb_closed_files
      << " file_close_duration = " << (end_ts - before_file_close_ts).count() / 1000 << " us");
}

std::filesystem::path WritingManager::getNextPath()
{
  // find path with most available space
  std::vector<fs::path>::iterator iPath = m_paths.begin();
  float requiredAvailFrac = m_config.writingPathMinAvailableFraction();
  std::vector<fs::path>::iterator iBestPath = m_paths.end();
  while (iPath != m_paths.end()) {
    // ignore current path
    if (*iPath == m_path) {
      iPath++;
      continue;
    }
    // get available space
    float availFrac;
    try {
      availFrac = getAvailableFraction(*iPath);
    } catch (WritingPathIssue& e) {
      ers::error(WritingManagerPathError(ERS_HERE, e));
      iPath = m_paths.erase(iPath);
      continue;
    }
    if (availFrac >= requiredAvailFrac) {
      requiredAvailFrac = availFrac;
      iBestPath = iPath;
    }
    iPath++;
  }

  if (iBestPath != m_paths.end()) {
    return *iBestPath;
  } else {
    float currAvailFrac = -1.;
    try {
      currAvailFrac = getAvailableFraction(m_path);
    } catch (WritingPathIssue& e) {
      ers::error(WritingManagerPathError(ERS_HERE, e));
      std::remove_if(m_paths.begin(), m_paths.end(),
          std::bind(std::equal_to<fs::path>(), std::placeholders::_1, m_path));
    }

    if (currAvailFrac >= requiredAvailFrac) {
      return m_path;
    } else {
      throw WritingManagerNoUsablePath(ERS_HERE);
    }
  }

}

void WritingManager::preparePaths()
{
  const std::vector<std::string>& pathStrings = m_config.writingPaths();

  m_paths.resize(pathStrings.size());
  std::partial_sort_copy(pathStrings.begin(), pathStrings.end(), m_paths.begin(), m_paths.end());

  std::size_t nOverfullPaths = 0;
  std::vector<fs::path>::iterator iPath = m_paths.begin();
  while (iPath != m_paths.end()) {
    float availFrac;
    try {
      // check duplicates
      if (iPath != m_paths.begin() && *(iPath - 1) == *iPath) {
        throw WritingPathDuplicate(ERS_HERE, *iPath);
      }
      // check existence
      if (!fs::exists(*iPath)) {
        throw WritingPathNotFound(ERS_HERE, *iPath);
      }
      // remove stale lock files
      if (fs::exists(*iPath / m_lockFileName)) {
        unlockPath(*iPath);
      }
      // try to write a new lock file to check if path is writable
      lockPath(*iPath);
      // remove test file
      unlockPath(*iPath);
      // get available space
      availFrac = getAvailableFraction(*iPath);
    } catch (WritingPathIssue& e) {
      ers::error(WritingManagerPathError(ERS_HERE, e));
      iPath = m_paths.erase(iPath);
      continue;
    }
    if (availFrac < m_config.writingPathMinAvailableFraction()) {
      ers::warning(
          WritingManagerPathWarning(ERS_HERE,
              WritingPathFreeSpaceTooLow(ERS_HERE, *iPath, availFrac, m_config.writingPathMinAvailableFraction())));
      nOverfullPaths++;
    }
    iPath++;
  }

  if (m_paths.size() == nOverfullPaths) {
    ers::warning(WritingManagerNoUsablePath(ERS_HERE));
  }

}

void WritingManager::startPathChange()
{
  class StartWritingPathChangeTask
  {
  public:
    StartWritingPathChangeTask(const std::string& newPath,
                               const WriterSerializerHandle& w) :
      m_newPath(newPath), m_writer(w)
    {
    }
    void operator()()
    {
      ERS_DEBUG(3, "StartWritingPathChangeTask: setting path to " << m_newPath
                << " for writer "
                << m_writer->tag().fullName()
                << " LB " << m_writer->tag().lumiblock);
      m_writer->ESWriter()->cd(m_newPath);
    }

    uint64_t weight() const { return 0; } // represents nothing wtt writing
    boost::posix_time::ptime timestamp() const { return boost::posix_time::microsec_clock::universal_time(); }

  private:
    const std::string& m_newPath;
    const WriterSerializerHandle m_writer;
  };

  try {
    lockPath(m_otherPath);
  } catch (WritingPathIssue& e) {
    ers::error(WritingManagerPathInfo(ERS_HERE, e));
  }

  tbb::queuing_rw_mutex::scoped_lock lock(m_mapsMutex, true);

  m_path.swap(m_otherPath);

  for (Map::const_iterator ci = m_map.begin(); ci != m_map.end(); ci++) {
    StartWritingPathChangeTask t(m_path.string(), ci->second);
    ci->second->post(t);
  }
}

void WritingManager::finishPathChange()
{
  class FinishWritingPathChangeTask
  {
  public:
    FinishWritingPathChangeTask(const WriterSerializerHandle& w) :
      m_writer(w)
    {
    }
    void operator()()
    {
      if (m_writer->ESWriter()->inTransition()) {
        ERS_DEBUG(3, "FinishWritingPathChangeTask: enforcing path change for writer "
            << m_writer->tag().fullName() << " LB " << m_writer->tag().lumiblock);
        m_writer->ESWriter()->closeFile();
      }
    }

    uint64_t weight() const { return 0; } // represents nothing wtt writing
    boost::posix_time::ptime timestamp() const { return boost::posix_time::microsec_clock::universal_time(); }

  private:
    const WriterSerializerHandle m_writer;
  };

  {
    tbb::queuing_rw_mutex::scoped_lock lock(m_mapsMutex, true);

    for (Map::const_iterator ci = m_map.begin(); ci != m_map.end(); ci++) {
      FinishWritingPathChangeTask t(ci->second);
      ci->second->post(t);
    }
  }
  // DirectoryManager makes unlockPath operations possibly very long (action timeout / 2)
  // so unlock the maps mutex, otherwise we can't process incoming data

  try {
    unlockPath(m_otherPath);
  } catch (WritingPathCannotUnlock& e) {
    ers::error(WritingManagerPathInfo(ERS_HERE, e));
  }
}

void WritingManager::applyUserCommands(bool & force_lb_close)
{
  /*
    switchDir [path]
    Immediately starts a disk rotation. Optionally the new path
    can be specified. It must be one of the programmed paths (use
    addPath otherwise)

    addPath <path>
    Adds a new path to the list of storage volumes

    removePath <path>
    Removes a path from the list of storage volumes

    rotationPeriod <period_min> [rotationTime_min]
    Changes the rotation period. Optionally the rotation overlap
    can be updated.

    maxLumiblockLifetime [seconds]
    Changes the time for which lumiblocks are kept open
    https://its.cern.ch/jira/browse/ADHI-4550
    https://its.cern.ch/jira/browse/ADHI-4558

    lbCloseMethodFallback [maxOpenLumiblocks]
    https://its.cern.ch/jira/browse/ADHI-4558
  */

  std::vector<std::string> cmd;

  while(m_commandQueue.try_pop(cmd)) {

    if (cmd.empty()) continue;

    std::string command = cmd.at(0);
    //Keep only the arguments
    cmd.erase(cmd.begin());

    try {
      if (command == "switchDir") {
        if (!m_otherPath.empty()) {
          UserCommandExecutionIssue
            issue(ERS_HERE,
                  "Storage transition is ongoing. Retry later.");
          ers::warning(issue);
          continue;
        }
        m_rotateNow = true;
        ERS_LOG("Initiating user-requested disk rotation.");
      }
      else if (command == "addPath") {
        fs::path path(cmd.at(0));
        if (std::find(m_paths.begin(), m_paths.end(), path) == m_paths.end()) {
          m_paths.push_back(path);
          ERS_LOG("Storage path added: " << path);
        }
      }
      else if (command == "removePath") {
        fs::path path(cmd.at(0));
        const auto& it = std::find(m_paths.begin(), m_paths.end(), path);
        if (it != m_paths.end()) {
          m_paths.erase(it);
          ERS_LOG("Storage path removed: " << path);
        }
      }
      else if (command == "rotationPeriod") {
        const auto& period = boost::lexical_cast<int>(cmd.at(0));
        m_rotationPeriod = pt::minutes(period);

        if (cmd.size() > 1) {
          const auto& overlap = boost::lexical_cast<int>(cmd.at(1));
          m_changeTime = pt::minutes(overlap);
        }
        ERS_LOG("Rotation parameters updated. Period (minutes): " <<
                m_rotationPeriod.minutes() << " Change time (minutes): " <<
                m_changeTime.minutes());
      }
      else if (command == "maxLumiblockLifetime") {
        uint32_t tmp, save = m_maxLumiblockLifetime;
        if (cmd.empty()) {
          // reset to initial configuration value
          tmp = m_config.maxLumiblockLifetime();
        } else {
          tmp = boost::lexical_cast<uint32_t>(cmd.at(0));
        }

        if (tmp != save) {
          m_maxLumiblockLifetime = tmp;
          ERS_INFO("maxLumiblockLifetime changed by user command from "
              << save << " to " << tmp);
          force_lb_close = true;
        }
      }
      else if (command == "lbCloseMethodFallback") {
        if (cmd.empty()) {
          m_lbCloseMethodFallback = false;
        } else {
          m_maxOpenLumiblock = boost::lexical_cast<uint32_t>(cmd.at(0));
          m_lbCloseMethodFallback = true;
        }
        // Note that this command can be used to force LB close: just close
        // with the current configuration
        ERS_INFO("lbCloseMethodFallback command: set to " << std::boolalpha
            << m_lbCloseMethodFallback << ", m_maxOpenLumiblock = "
            << m_maxOpenLumiblock);
        force_lb_close = true;
      }
    } catch(const std::out_of_range& ex) {
      const auto& args = boost::algorithm::join(cmd, " ");
      UserCommandParsingIssue issue(ERS_HERE, command, args, ex);
      ers::warning(issue);
    } catch(const boost::bad_lexical_cast& ex) {
      const auto& args = boost::algorithm::join(cmd, " ");
      UserCommandParsingIssue issue(ERS_HERE, command, args, ex);
      ers::warning(issue);
    }
  }
}

void WritingManager::tracker()
{
  pt::ptime lastLumiblockClosing = pt::second_clock::universal_time();
  pt::ptime lastPathChange = pt::second_clock::universal_time();

  //Apply the randomization offset
  lastPathChange -= m_config.writingPathLifetimeOffset();

  m_rotationPeriod = m_config.writingPathLifetime();
  m_changeTime = m_config.writingPathChangeTime();
  m_rotateNow = false;

  std::chrono::seconds const tracker_granularity_s(m_config.filesystemTrackerGranularity().total_seconds());

  std::unique_lock<std::mutex> lock(m_trackerRunningMutex);
  while (m_trackerRunning) {

    //Check for new user commands here
    bool force_lb_close = false;
    applyUserCommands(force_lb_close);

    if (force_lb_close) {
      ERS_LOG("forcing close of old lumiblocks");
      lastLumiblockClosing = pt::second_clock::universal_time();
      closeOldLumiblocks();
      ERS_LOG("forcing close of old lumiblocks: done");
    }


    if (m_trackerRunningCond.wait_for(lock, tracker_granularity_s) == std::cv_status::timeout) {
      ERS_DEBUG(4, "WritingManager: tracker timeout expired: checking if work is needed!");
      pt::ptime now = pt::second_clock::universal_time();

      if ((now - lastLumiblockClosing) >= m_config.filesystemTrackerGranularity()) {
        lastLumiblockClosing = now;
        closeOldLumiblocks();
      }

      if (m_paths.size() > 1) { // no rotation if only one path
        if (m_otherPath.empty()) { // no path rotation in progress
          float currAvailFrac;
          try {
            currAvailFrac = getAvailableFraction(m_path);
          } catch (WritingPathIssue& e) {
            // there's something very wrong with the current path
            // we will try to rotate it
            currAvailFrac = -1.;
          }
          if (m_isXoff || // XOFF
              (currAvailFrac < m_config.writingPathMinAvailableFraction()) || // running out of space
              ((now - lastPathChange) >= m_rotationPeriod) || // time to rotate paths
              m_rotateNow) { //User command

            m_rotateNow = false;
            // try to rotate paths
            lastPathChange = now;
            try {
              m_otherPath = getNextPath();
            } catch (WritingManagerNoUsablePath& e) {
              ers::error(e);
              m_isXoff = true;
            }
            if (!m_otherPath.empty()) {
              startPathChange();
              m_isXoff = false;
            }
          }
        } else { // path rotation in progress
          if ((now - lastPathChange) >= m_changeTime) { // time to finish path rotation
            finishPathChange();
            m_otherPath.clear();
          }
        }
      }
    } // if (!m_trackerRunningCond.timed_wait(lock, m_config.filesystemTrackerGranularity()))
  } // while (m_trackerRunning)
}

} // namespace SFOng
