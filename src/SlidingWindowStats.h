#ifndef SLIDINGWINDOWSTATS_H
#define SLIDINGWINDOWSTATS_H

#include "boost/date_time/posix_time/posix_time.hpp"
#include <deque>
#include <mutex>

/* Thread-safe class for maintaining timed values over a defined period and
 * compute some reductions over it.
 *
 * Data outside of the time period ]now, now-period] is erased
 * each time an action is performed (push or stats computations).
 *
 * Note that, data age are evaluated against TClock current time, so if you
 * push data with timestamp from a different clock or with a significant
 * difference, you may have unexpected behavior.
 */
template <typename T> class SlidingWindowStats
{
public:
  using TPeriod = boost::posix_time::time_duration;
  using TTimestamp = boost::posix_time::ptime;
  using TClock = boost::posix_time::microsec_clock;

  static int constexpr DEFAULT_PERIOD_S = 5;

  SlidingWindowStats(TPeriod period = boost::posix_time::seconds(DEFAULT_PERIOD_S));
  // TBB asks for MoveConstructible
  SlidingWindowStats(SlidingWindowStats<T> &&);

  void setPeriod(TPeriod period);

  void push(TTimestamp ts, T data);

  // "reductions" are not const because data might be erased before computation
  T sum();

  void dump(std::ostream & os) const;

private:
  TPeriod m_period;

  typedef std::pair<TTimestamp, T> TTimedData;
  std::deque<TTimedData> m_data; // oldest first

  std::mutex mutable m_mutex;

  // Caller must hold the lock while calling clearOldData
  void clearOldData();
};

template <typename T> SlidingWindowStats<T>::SlidingWindowStats(TPeriod period)
  : m_period(period)
{
}

template <typename T> SlidingWindowStats<T>::SlidingWindowStats(SlidingWindowStats<T> && a)
  : m_period(std::move(a.m_period))
  , m_data(std::move(a.m_data))
  , m_mutex()
{
}

template <typename T> void SlidingWindowStats<T>::setPeriod(TPeriod period)
{
  if (period < m_period) {
    m_period = period;
    clearOldData();
  } else {
    m_period = period;
  }
}

template <typename T> void SlidingWindowStats<T>::push(TTimestamp ts, T data)
{
  std::lock_guard<std::mutex> lock(m_mutex);

  m_data.push_back(std::make_pair(ts, data));

  clearOldData();
}

template <typename T> T SlidingWindowStats<T>::sum()
{
  T sum = 0;

  std::lock_guard<std::mutex> lock(m_mutex);

  clearOldData();

  for (TTimedData const & d: m_data) {
    sum += d.second;
  }

  return sum;
}

template <typename T> void SlidingWindowStats<T>::clearOldData()
{
  if (m_data.empty())
    return;

  TTimestamp const limit = boost::posix_time::microsec_clock::universal_time() - m_period;

  while (!m_data.empty() && m_data.front().first <= limit)
    m_data.pop_front();
}

template <typename T> void SlidingWindowStats<T>::dump(std::ostream & os) const
{
  std::lock_guard<std::mutex> lock(m_mutex);

  for (TTimedData const & d: m_data) {
    os << boost::posix_time::to_simple_string(d.first) << ": " << d.second << ", ";
  }
  os << std::endl;
}

#endif /* SLIDINGWINDOWSTATS_H */
