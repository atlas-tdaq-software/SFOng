#include "DirectoryManager.h"
#include <chrono>
#include <filesystem>
#include <ostream>
#include <thread>
#include "ers/ers.h"

DirectoryManager::DirectoryManager()
{
}

DirectoryManager::~DirectoryManager()
{
}

void DirectoryManager::db_updated(SFOTZ::DBInfoPointer update_info)
{
  // By subscription, we should receive only updates for file closing, but better be safe
  if (update_info->command != SFOTZ::DBUpdateInfo::FILECLOSED) {
    ERS_LOG("DirectoryManager: received unsolicited command: type= " << static_cast<int>(update_info->command));
    return;
  }

  ERS_DEBUG(1, "DirectoryManager: file was closed in DB: " << update_info->sfopfn);

  std::string const dir = std::filesystem::path(update_info->sfopfn).parent_path();

  std::lock_guard<std::mutex> lock(dir_files_mutex);
  TDirectoryFiles::iterator dir_it = dir_files.find(dir);
  if (dir_it != dir_files.end()) {
    dir_it->second.erase(update_info->sfopfn);
  }
}

void DirectoryManager::FileWasOpened(std::string logicalfileName, std::string fileName, std::string /*streamtype*/,
    std::string /*streamname*/, std::string /*sfoid*/, std::string /*guid*/, unsigned int /*runNumber*/,
    unsigned int /*filenumber*/, unsigned int /*lumiblock*/)
{
  // fileName: path and file name with ".writing" extension
  // logicalfileName: the final filename with ".data" extension

  ERS_DEBUG(1, "DirectoryManager: file was opened: " << fileName);
  std::string const dir = std::filesystem::path(fileName).parent_path();

  std::lock_guard<std::mutex> lock(dir_files_mutex);
  dir_files[dir].insert(dir + '/' + logicalfileName);
}

void DirectoryManager::FileWasClosed(std::string, std::string, std::string, std::string, std::string,
    std::string, std::string, unsigned int, unsigned int, unsigned int,
    unsigned int, uint64_t)
{
  // NOOP: we don't care about file closing
}

bool DirectoryManager::wait_for_empty_dir(std::string const & dir, unsigned timeout_ms)
{
  ERS_DEBUG(0, "DirectoryManager::wait_for_empty_dir: " << dir << ", " << timeout_ms);
  static unsigned const TIMEOUT_RESOLUTION_MS = 100;
  unsigned waited = 0;

  while (waited < timeout_ms) {
    std::unique_lock<std::mutex> lock(dir_files_mutex);
    if (dir_files[dir].empty()) {
      ERS_DEBUG(0, "DirectoryManager::wait_for_empty_dir: directory now empty: " << dir);
      return true;
    }
    ERS_DEBUG(1, "DirectoryManager::wait_for_empty_dir: directory not empty: " << dir << ", number of remaining entries: " << dir_files[dir].size());
    lock.unlock();

    std::this_thread::sleep_for(std::chrono::milliseconds(TIMEOUT_RESOLUTION_MS));
    waited += TIMEOUT_RESOLUTION_MS;
  }

  // Check one last time: also handles the "timeout_ms = 0" case
  std::unique_lock<std::mutex> lock(dir_files_mutex);
  if (dir_files[dir].empty()) {
    ERS_DEBUG(0, "DirectoryManager::wait_for_empty_dir: directory now empty 2: " << dir);
    return true;
  }

  ERS_LOG("DirectoryManager::wait_for_empty_dir: timeout: " << dir << " number of remaining entries: " << dir_files[dir].size());
  return false;
}

void DirectoryManager::clear_dir_entries(std::string const & dir)
{
  ERS_DEBUG(0, "DirectoryManager::clear_dir_entries: " << dir);
  std::lock_guard<std::mutex> lock(dir_files_mutex);
  dir_files[dir].clear();
}
