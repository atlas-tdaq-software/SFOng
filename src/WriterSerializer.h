#ifndef WRITERSERIALIZER_H
#define WRITERSERIALIZER_H

#include <cstdint>
#include <boost/asio/io_service.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/post.hpp>
#include <filesystem>
#include <memory>
#include "Tag.h"
#include "WeightedIOService.h"
#include "src/SlidingWindowStats.h"
namespace EventStorage { class DataWriter; }
namespace EventStorage { class DataWriterCallBack; }
namespace SFOng { class Config; }
namespace SFOng { class SfotzPublisher; }

namespace SFOng
{

class WriterSerializer
{

public:

  WriterSerializer(const Config& config,
                   WeightedIOService& io_service,
                   SfotzPublisher& sfotzPublisher,
                   const Tag& tag,
                   const std::filesystem::path& path,
                   const int index,
                   SlidingWindowStats<uint64_t> & stream_weight,
                   EventStorage::DataWriterCallBack* dir_manager);

  ~WriterSerializer();

  inline const Tag& tag() const
  {
    return m_tag;
  }
  inline EventStorage::DataWriter* ESWriter()
  {
    return m_writer.get();
  }

  template<typename T> void post(T && t)
  {
    auto const ts = t.timestamp();
    uint64_t const weight = t.weight();
    m_ioService.addWeight(ts, weight);
    m_streamWeight.push(ts, weight);
    boost::asio::post(m_strand, t);
  }

private:

  boost::asio::io_service::strand m_strand;
  WeightedIOService & m_ioService;
  SfotzPublisher& m_sfotzPublisher;
  Tag m_tag;
  std::unique_ptr<EventStorage::DataWriter> m_writer;
  SlidingWindowStats<uint64_t> & m_streamWeight;
};

} // namespace SFOng

#endif // WRITERSERIALIZER_H
