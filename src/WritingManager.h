#ifndef WRITINGMANAGER_H
#define WRITINGMANAGER_H

#include <tbb/concurrent_hash_map.h>
#include <tbb/concurrent_queue.h>
#include <tbb/queuing_rw_mutex.h>
#include <atomic>
#include <boost/asio/io_service.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <filesystem>
#include <chrono>
#include <cstdint>
#include <deque>
#include <list>
#include <mutex>
#include <condition_variable>
#include <ostream>
#include <string>
#include <thread>
#include <vector>
#include "DirectoryManager.h"
#include "RunControl/Common/Controllable.h"
#include "Tag.h"
#include "Types.h"
#include "WeightedIOService.h"
#include "ers/Issue.h"
#include "src/SlidingWindowStats.h"
namespace SFOng { class Config; }
namespace SFOng { class SfotzPublisher; }
namespace SFOng { class StatsCollector; }
namespace daq { namespace rc { class TransitionCmd; } }
namespace daq { namespace rc { class UserCmd; } }

namespace SFOng
{

class WritingManager: public daq::rc::Controllable
{

public:

  WritingManager(const Config& config, SfotzPublisher& sfotzPublisher,
                 StatsCollector& sc);
  ~WritingManager() noexcept {}

  void configure(const daq::rc::TransitionCmd&) override;
  void prepareForRun(const daq::rc::TransitionCmd&) override;
  void stopRecording(const daq::rc::TransitionCmd&) override;
  void unconfigure(const daq::rc::TransitionCmd&) override;
  void user(const daq::rc::UserCmd&) override;

  void addEvent(EventHandle event);
  
  inline bool isXoff()
  {
    return m_isXoff;
  }
  

private:

  typedef tbb::concurrent_hash_map<Tag, WriterSerializerHandle> Map;
  typedef tbb::concurrent_hash_map<Tag, unsigned int> IndexMap;
  typedef tbb::concurrent_hash_map<Tag, SlidingWindowStats<uint64_t>, TbbTagHasherNolb> StreamWeightMap;

  bool checkLate(uint16_t lumiblock);
  WriterSerializerHandle getWriter(const Tag& tag);

  static float getAvailableFraction(const std::filesystem::path& path);
  void lockPath(const std::filesystem::path& path);
  void unlockPath(const std::filesystem::path& path);

  void applyUserCommands(bool & force_lb_close);

  void closeOldLumiblocks();
  std::filesystem::path getNextPath();
  void preparePaths();
  void startPathChange();
  void finishPathChange();
  void tracker();

  const Config& m_config;
  std::atomic<bool> m_isXoff;
  Map m_map;
  IndexMap m_indexmap;
  StreamWeightMap m_streamWeightMap;

  // Events with lumiblock smaller then this are declared late. The associated
  // file is most likely already closed.
  std::atomic<uint16_t> m_lumiblockLimit;
  // Lifetime in second after which a lumiblock will be closed.
  uint32_t m_maxLumiblockLifetime;
  bool m_lbCloseMethodFallback;
  uint32_t m_maxOpenLumiblock;

  StatsCollector& m_statsCollector;
  SfotzPublisher& m_sfotzPublisher;
  std::string m_lockFileName;
  bool m_trackerRunning;
  std::mutex m_trackerRunningMutex;
  std::condition_variable m_trackerRunningCond;
  std::thread* m_trackerThread;
  std::filesystem::path m_path;
  std::filesystem::path m_otherPath;
  std::vector<std::filesystem::path> m_paths;
  boost::posix_time::time_duration m_rotationPeriod;
  boost::posix_time::time_duration m_changeTime;
  bool m_rotateNow;
  tbb::concurrent_bounded_queue<std::vector<std::string>> m_commandQueue;
  tbb::queuing_rw_mutex m_mapsMutex;
  tbb::queuing_rw_mutex m_lumiblockMutex;

  std::list<WeightedIOService> m_ioServices;
  std::vector<boost::asio::io_service::work> m_ioServiceWorks;
  std::vector<std::thread> m_ioServiceThreads;
  std::list<WeightedIOService>::iterator m_nextIoService;
  std::mutex m_getWriterMutex;

  boost::posix_time::time_duration m_streamWeightPeriod;

  typedef std::deque<
      std::pair<uint16_t, std::chrono::time_point<std::chrono::steady_clock>>
      > TLumiblockTimes;
  TLumiblockTimes m_lumiblockTimes;

  std::unique_ptr<DirectoryManager> m_dir_manager;
};

} // namespace SFOng

ERS_DECLARE_ISSUE(SFOng,
    WritingManagerNoUsablePath,
    "None of the configured writing paths has enough available space. "
    "Unless more space becomes available, this SFO will not accept data.",
    ERS_EMPTY)

ERS_DECLARE_ISSUE(SFOng,
    WritingManagerPathInfo,
    "One of the configured writing paths has problems, but will nevertheless be used.",
    ERS_EMPTY)

ERS_DECLARE_ISSUE(SFOng,
    WritingManagerPathWarning,
    "One of the configured writing paths has problems and will be ignored until they are resolved.",
    ERS_EMPTY)

ERS_DECLARE_ISSUE(SFOng,
    WritingManagerPathError,
    "One of the configured writing paths has problems and will be permanently ignored.",
    ERS_EMPTY)

ERS_DECLARE_ISSUE(SFOng,
    UserCommandIssue,
    "Problem with a user command.",
    ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(SFOng,
    UserCommandParsingIssue,
    UserCommandIssue,
    "Parsing of user command failed. Command: '" 
                  << cmd << "' Args: '" << args << "'",
    ERS_EMPTY,
    ((std::string) cmd) ((std::string) args))

ERS_DECLARE_ISSUE_BASE(SFOng,
    UserCommandExecutionIssue,
    UserCommandIssue,
    "Cannot execute user command. Reason: " << reason,
    ERS_EMPTY,
    ((std::string) reason))

ERS_DECLARE_ISSUE(SFOng,
    WritingPathIssue,
    " Path: " << path,
    ((std::filesystem::path) path))

ERS_DECLARE_ISSUE_BASE(SFOng,
    WritingPathCannotLock,
    WritingPathIssue,
    "Cannot create lock file.",
    ((std::filesystem::path) path),
    ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(SFOng,
    WritingPathCannotUnlock,
    WritingPathIssue,
    "Cannot delete lock file.",
    ((std::filesystem::path) path),
    ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(SFOng,
    WritingPathFreeSpaceError,
    WritingPathIssue,
    "Cannot determine available space.",
    ((std::filesystem::path) path),
    ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(SFOng,
    WritingPathFreeSpaceTooLow,
    WritingPathIssue,
    "Not enough available space (free: " << (int)(free * 100) << "%, needed: " << (int)(needed * 100) << "%).",
    ((std::filesystem::path) path),
    ((float) free) ((float) needed))

ERS_DECLARE_ISSUE_BASE(SFOng,
    WritingPathDuplicate,
    WritingPathIssue,
    "Path is specified multiple times.",
    ((std::filesystem::path) path),
    ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(SFOng,
    WritingPathNotFound,
    WritingPathIssue,
    "Path does not exist.",
    ((std::filesystem::path) path),
    ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(SFOng,
    WritingPathNotWritable,
    WritingPathIssue,
    "Path is not writable.",
    ((std::filesystem::path) path),
    ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(SFOng,
    UnlockedWritingPathUnsynced,
    WritingPathIssue,
    "Unlocked writing path but there are still unpublished files.",
    ((std::filesystem::path) path),
    ERS_EMPTY)

#endif // WRITINGMANAGER_H
