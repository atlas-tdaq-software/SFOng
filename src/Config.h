#ifndef CONFIG_H
#define CONFIG_H

#include <cstdint>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <chrono>
#include <iosfwd>
#include <mutex>
#include <string>
#include <vector>
#include "DFdal/DFParameters.h"
#include "EventStorage/EventStorageRecords.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/OnlineServices.h"
#include "SFOng/dal/SFOngApplication.h"
#include "SFOng/dal/SFOngConfiguration.h"
#include "Tag.h"
#include "config/Configuration.h"
#include "config/DalObject.h"
#include "dal/DataFlowParameters.h"
#include "dal/Partition.h"
#include "ers/Issue.h"
#include "ipc/partition.h"
namespace daq { namespace core { class Segment; } }
namespace daq { namespace df { class SFODBConnection; } }
namespace daq { namespace rc { class TransitionCmd; } }

namespace SFOng
{

class StopMutex
{
 public:
  static std::mutex& getMutex()
  {
    static std::mutex * mutex = new std::mutex();
    return *mutex;
  }

 private:
  StopMutex();
};

enum class AssignPolicy
{
  RoundRobin,
  OccupancyWeighted,
  DebugAssignErrors
};

class Config: public daq::rc::Controllable
{

public:

  Config();

  void configure(const daq::rc::TransitionCmd&) override;
  void prepareForRun(const daq::rc::TransitionCmd&) override;
  void stopRecording(const daq::rc::TransitionCmd&) override;
  void unconfigure(const daq::rc::TransitionCmd&) override;

  inline const boost::posix_time::time_duration& actionTimeout() const
  {
    return m_derivedParams.actionTimeout;
  }
  inline const std::string applicationName() const
  {
    return daq::rc::OnlineServices::instance().applicationName();
  }
  inline const std::string UID() const
  {
    return application()->UID();
  }
  inline uint32_t bufferSize_KiB() const
  {
    return configuration()->get_BufferSize_KiB();
  }
  inline bool compressionEnabled() const
  {
    return (configuration()->get_CompressionAlgorithm() == "ZLIB");
  }
  inline uint16_t compressionLevel() const
  {
    return configuration()->get_CompressionLevel();
  }
  inline const Tag& corruptEventsTag() const
  {
    return m_derivedParams.corruptEventsTag;
  }
  inline const std::chrono::milliseconds eventTransferTimeout() const
  {
    return m_derivedParams.eventTransferTimeout;
  }
  const boost::posix_time::time_duration& filesystemTrackerGranularity() const
  {
    return m_derivedParams.filesystemTrackerGranularity;
  }
  inline const IPCPartition& ipcPartition() const
  {
    return m_derivedParams.ipcPartition;
  }
  inline const Tag& lateEventsTag() const
  {
    return m_derivedParams.lateEventsTag;
  }
  inline bool lumiblockEnabled() const
  {
    return configuration()->get_LumiblockEnabled();
  }
  inline uint32_t maxEventsPerFile() const
  {
    return configuration()->get_MaxEventsPerFile();
  }
  inline uint32_t maxFileSize_MiB() const
  {
    return configuration()->get_MaxFileSize_MiB();
  }
  inline uint32_t maxLumiblockLifetime() const
  {
    return configuration()->get_MaxLumiblockLifetime();
  }
  inline const std::vector<std::string>& metaDataStrings() const
  {
    return m_derivedParams.metaDataStrings;
  }
  inline uint32_t nBuffers() const
  {
    return configuration()->get_NBuffers();
  }
  inline uint32_t nInputThreads() const
  {
    return m_derivedParams.nInputThreads;
  }
  inline uint16_t nSfos() const
  {
    return m_computedParams.nSfos;
  }
  inline uint16_t nWorkerThreads() const
  {
    return m_derivedParams.nWorkerThreads;
  }
  inline const std::string& projectTag() const
  {
    return m_derivedParams.projectTag;
  }
  inline const daq::df::SFODBConnection* sfotzDatabaseConnection() const
  {
    return configuration()->get_SFOTZDBConnection();
  }
  const boost::posix_time::time_duration& statsSamplerGranularity() const
  {
    return m_derivedParams.statsSamplerGranularity;
  }
  inline const EventStorage::run_parameters_record& runParams() const
  {
    return m_derivedParams.runParams;
  }
  inline bool writingEnabled() const
  {
    return m_computedParams.writingEnabled;
  }
  inline const boost::posix_time::time_duration& writingPathChangeTime() const
  {
    return m_derivedParams.writingPathChangeTime;
  }
  inline const boost::posix_time::time_duration& writingPathLifetime() const
  {
    return m_derivedParams.writingPathLifetime;
  }
  inline const boost::posix_time::time_duration& 
    writingPathLifetimeOffset() const
  {
    return m_derivedParams.writingPathLifetimeOffset;
  }
  inline float writingPathMinAvailableFraction() const
  {
    return configuration()->get_DirectoryMinFreeSpace();
  }
  inline const std::vector<std::string>& writingPaths() const
  {
    return configuration()->get_DataDirectories();
  }
  inline const std::vector<std::string>& dataNetworks() const
  {
    const daq::df::DFParameters *dfparams = 
      database().cast<daq::df::DFParameters>(partition()->get_DataFlowParameters());
    return dfparams->get_DefaultDataNetworks();
  }
  inline const dal::SFOngApplication* application() const
  {
    return database().cast<dal::SFOngApplication>(
        &daq::rc::OnlineServices::instance().getApplication());
  }
  inline Configuration& database() const
  {
    return daq::rc::OnlineServices::instance().getConfiguration();
  }

  AssignPolicy ioServiceAssignPolicy() const
  {
    return m_derivedParams.ioServiceAssignPolicy;
  }

  uint32_t streamWeightPeriod() const
  {
    return m_derivedParams.streamWeightPeriod;
  }

  uint32_t ioServiceWeightPeriod() const
  {
    return m_derivedParams.ioServiceWeightPeriod;
  }

  TWatchedStreams const & assignPolicyWatchedStreams() const
  {
    return m_secretParams.assignPolicyWatchedStreams;
  }

  inline int adler32MtNbThreads() const
  {
    return configuration()->get_Adler32MtNbThreads();
  }

  inline uint32_t adler32MtThreshold() const
  {
    return configuration()->get_Adler32MtThreshold();
  }

  inline bool unlockDirDbSync() const
  {
    return configuration()->get_UnlockDirDbSync();
  }

private:

  inline const dal::SFOngConfiguration* configuration() const
  {
    return application()->get_SFOngConfiguration();
  }
  inline const daq::core::Partition* partition() const
  {
    return &daq::rc::OnlineServices::instance().getPartition();
  }
  inline const daq::core::Segment* segment() const
  {
    return &daq::rc::OnlineServices::instance().getSegment();
  }
  void parseSecretParams();

  struct
  {
    unsigned int fakeRunNo;
    uint16_t nSfos;
    bool writingEnabled;
  } m_computedParams;
  struct
  {
    boost::posix_time::time_duration actionTimeout;
    Tag corruptEventsTag;
    boost::posix_time::time_duration filesystemTrackerGranularity;
    std::chrono::milliseconds eventTransferTimeout;
    IPCPartition ipcPartition;
    Tag lateEventsTag;
    std::vector<std::string> metaDataStrings;
    std::uint32_t nInputThreads;
    std::uint32_t nWorkerThreads;
    std::string projectTag;
    EventStorage::run_parameters_record runParams;
    boost::posix_time::time_duration statsSamplerGranularity;
    boost::posix_time::time_duration writingPathChangeTime;
    boost::posix_time::time_duration writingPathLifetime;
    boost::posix_time::time_duration writingPathLifetimeOffset;
    AssignPolicy ioServiceAssignPolicy;
    uint32_t streamWeightPeriod;
    uint32_t ioServiceWeightPeriod;
  } m_derivedParams;
  struct
  {
    std::string corruptEventsTagName;
    std::string corruptEventsTagType;
    uint32_t filesystemTrackerGranularity_s;
    bool randomizeRotation;
    std::string lateEventsTagName;
    std::string lateEventsTagType;
    uint32_t statsSamplerGranularity_ms;
    TWatchedStreams assignPolicyWatchedStreams;
  } m_secretParams;

};

} // namespace SFOng

std::ostream & operator<<(std::ostream & os, SFOng::AssignPolicy const & p);

ERS_DECLARE_ISSUE(SFOng, ConfigIssue, ERS_EMPTY, ERS_EMPTY)

#endif // CONFIGURATION_H
