#ifndef INPUT_H
#define INPUT_H

#include <cstdint>
#include <atomic>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <chrono>
#include <deque>
#include <list>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>
#include "RunControl/Common/Controllable.h"
#include "Types.h"
namespace SFOng { class Config; }
namespace SFOng { class StatsCollector; }
namespace SFOng { class WritingManager; }
namespace boost { namespace system { class error_code; } }
namespace daq { namespace rc { class TransitionCmd; } }
namespace ers { class Issue; }

namespace SFOng
{

class Input: public daq::rc::Controllable
{

public:

  class Server;
  class Session;

  friend class Server;
  friend class Session;

  Input(const Config& config, WritingManager& wm, StatsCollector& sc);
  ~Input() noexcept {}

  void configure(const daq::rc::TransitionCmd&) override;
  void prepareForRun(const daq::rc::TransitionCmd&) override;
  void stopRecording(const daq::rc::TransitionCmd&) override;
  void unconfigure(const daq::rc::TransitionCmd&) override;
  void publish() override;

  bool isRunning() { return m_running; }

  void returnBuffer(BufferHandle buffer);

private:

  void onServerAccept(std::shared_ptr<Session> session);
  void onServerAcceptError(const boost::system::error_code& error,
      std::shared_ptr<Session> session);
  void onSessionTransfer(EventHandle event);
  void onSessionTransferError(const ers::Issue& error,
      BufferHandle buffer);
  void onSessionClose(std::shared_ptr<Session> session);
  void onTimer(const boost::system::error_code& error);

  void distributeBuffers(BufferHandle returnedBuffer = nullptr);

  const Config& m_config;
  WritingManager& m_writingManager;
  StatsCollector& m_statsCollector;

  std::atomic<bool> m_running;

  std::list<boost::asio::io_service> m_ioServices;
  std::vector<boost::asio::io_service::work> m_ioServiceWorks;
  std::vector<std::thread> m_ioServiceThreads;
  std::list<boost::asio::io_service>::iterator m_nextIoService;

  std::shared_ptr<Server> m_server;
  std::vector<std::shared_ptr<Session>> m_sessions;
  std::mutex m_sessionsMutex;

  std::vector<BufferHandle> m_freeBuffers;
  std::mutex m_freeBuffersMutex;

  std::unique_ptr<boost::asio::steady_timer> m_timer;
};

} // namespace SFOng

#endif // INPUT_H
