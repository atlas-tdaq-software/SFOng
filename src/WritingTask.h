#ifndef WRITINGTASK_H
#define WRITINGTASK_H

#include <cstdint>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <ostream>
#include <string>
#include "Types.h"
#include "ers/Issue.h"
namespace SFOng { class EventStream; }
namespace SFOng { class StatsCollector; }

namespace SFOng
{

class WritingTask
{

public:

  WritingTask(StatsCollector& sc, const WriterSerializerHandle& ws,
      const EventHandle& event, const EventStream& stream);

  void operator()();

  // how heavy is the task from a writing point of view
  uint64_t weight() const;
  boost::posix_time::ptime timestamp() const;

private:
  StatsCollector& m_statsCollector;
  const WriterSerializerHandle m_writer;
  const EventHandle m_event;
  const EventStream& m_stream;
  boost::posix_time::ptime m_creationTimestamp;
};

} // namespace SFOng

ERS_DECLARE_ISSUE(SFOng,
    WritingTaskIssue,
    "Problem writing event " << gid << " to stream " << tagFullName << ".",
    ((uint32_t) gid) ((std::string) tagFullName))

#endif // WRITINGTASK_H
