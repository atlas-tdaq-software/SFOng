#ifndef SFOTZPUBLISHER_H
#define SFOTZPUBLISHER_H

#include <filesystem>
#include <mutex>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "RunControl/Common/Controllable.h"
#include "SFOTZ/DBUpdateInfo.h"
#include "SFOTZ/OverlapTable.h"
#include "StatsCollector.h"
#include "Tag.h"
#include "ers/Issue.h"
class TH2I;
namespace EventStorage { class DataWriterCallBack; }  // lines 20-20
namespace SFOTZ { class SFOTZThread; }
namespace SFOTZ { struct SFOTZCallback; }
namespace SFOng { class Config; }
namespace daq { namespace rc { class TransitionCmd; } }

namespace SFOng
{

class SfotzPublisher: public daq::rc::Controllable
{

public:

  SfotzPublisher(const Config& config, const StatsCollector& stats);

  void prepareForRun(const daq::rc::TransitionCmd&) override;
  void stopRecording(const daq::rc::TransitionCmd&) override;
  void unconfigure(const daq::rc::TransitionCmd&) override;

  inline EventStorage::DataWriterCallBack* dataWriterCallback()
  {
    return m_callback;
  }

  void onWriterCreation(const Tag& tag);
  void onWriterDestruction(const Tag& tag);

  void publishLumiblockClosed(const Tag& tag);

  void moveStaleFiles();
  unsigned int staleTags(std::unordered_map<Tag, unsigned int>& staleTags);

  // must be called after prepareForRun
  bool registerSFOTZCallbackToMainDbThread(SFOTZ::SFOTZCallback* cb_obj, std::set<SFOTZ::DBUpdateInfo::CommandType> const & command_types);

private:

  void publishLumiblockOpened(const Tag& tag);
  void publishRunOpened(const Tag& tag);
  void publishRunClosed(const Tag& tag);
  void publishInfo(const SFOTZ::DBInfoPointer& info);
  void publishEoEStats();
  void parseHistoAndPublish(TH2I* histo, SFOTZ::Overlap::Types type);

  EventStorage::DataWriterCallBack* m_callback;
  const Config& m_config;
  std::unordered_set<Tag> m_runTags;
  std::mutex m_runTagsMutex;
  const StatsCollector& m_stats;
  std::vector<SFOTZ::SFOTZThread*> m_threads;

};

} // namespace SFOng


ERS_DECLARE_ISSUE(SFOng,
    FileNotFound,
    "Cannot find file during recovery procedure. Filename: " << path,
    ((std::filesystem::path) path))

ERS_DECLARE_ISSUE(SFOng,
    IncompleteDatabase,
    "Transition timeout: could not complete the publication in SFO database; remaining entries: " << remaining_entries,\
    ((unsigned int)remaining_entries))

#endif // SFOTZPUBLISHER_H
