#ifndef STATSCOLLECTOR_H
#define STATSCOLLECTOR_H

#include <tbb/concurrent_queue.h>
#include <tbb/concurrent_unordered_map.h>
#include <atomic>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <cstdint>
#include <memory>
#include <mutex>
#include <string>
#include <vector>
#include "Histograms.h"
#include "RunControl/Common/Controllable.h"
#include "monsvc/PublishingController.h"
#include "monsvc/ptr.h"
namespace SFOng { class Config; }
namespace SFOng { class Event; }
namespace SFOng { struct EventStream; }
namespace SFOng { namespace info { class SFOngCounters; } }
namespace SFOng { namespace info { class SFOngInput; } }
namespace daq { namespace rc { class TransitionCmd; } }

namespace SFOng
{

class StatsCollector: public daq::rc::Controllable
{

public:

  struct Counters
  {
    Counters();

    std::vector<std::pair<boost::posix_time::ptime, uint32_t> > counts;
    std::vector<boost::posix_time::time_duration> lifeTimes;
  };

  struct ConcurrentCounters
  {
    ConcurrentCounters();
    ConcurrentCounters(ConcurrentCounters const & rhs)
    	: counts(rhs.counts)
    	, lifeTimes(rhs.lifeTimes)
    {
    	count.store(rhs.count.load());
    }

    void fillCounters(Counters& c);

    std::atomic<uint32_t> count;
    tbb::concurrent_bounded_queue<std::pair<boost::posix_time::ptime, uint32_t> > counts;
    tbb::concurrent_bounded_queue<boost::posix_time::time_duration> lifeTimes;
  };

  struct TaskCounters: public Counters
  {
    TaskCounters();

    uint64_t totalEvents;
    uint64_t totalSize;
    uint64_t totalUncompressedSize;
    std::vector<boost::posix_time::time_duration> runTimes;

    /* Values at the beginning of the period, so that
     * (totalX - lastTotalX) / elapsed_time = rateX
     */
    uint64_t lastTotalEvents;
    uint64_t lastTotalSize;
    uint64_t lastTotalUncompressedSize;
  };

  struct ConcurrentTaskCounters: public ConcurrentCounters
  {
    ConcurrentTaskCounters();
    ConcurrentTaskCounters(ConcurrentTaskCounters const & rhs)
    	: ConcurrentCounters(rhs)
    	, runTimes(rhs.runTimes)
    {
    	totalEvents.store(rhs.totalEvents.load());
    	totalSize.store(rhs.totalSize.load());
    	totalUncompressedSize.store(rhs.totalUncompressedSize.load());
    	lastTotalEvents.store(rhs.lastTotalEvents.load());
    	lastTotalSize.store(rhs.lastTotalSize.load());
    	lastTotalUncompressedSize.store(rhs.lastTotalUncompressedSize.load());
    }
    void fillCounters(TaskCounters& c);

    std::atomic<uint64_t> totalEvents;
    std::atomic<uint64_t> totalSize;
    std::atomic<uint64_t> totalUncompressedSize;
    tbb::concurrent_bounded_queue<boost::posix_time::time_duration> runTimes;

    /* Values as of last time fillCounters was called
     * We keep these to be able to compute event and data rates over the last
     * period.
     */
    std::atomic<uint64_t> lastTotalEvents;
    std::atomic<uint64_t> lastTotalSize;
    std::atomic<uint64_t> lastTotalUncompressedSize;
  };

  struct CountersInfo
  {
    CountersInfo();

    monsvc::ptr<info::SFOngCounters> info;
    boost::posix_time::ptime timestamp;
    std::shared_ptr<std::once_flag> once;
  };

public:

  StatsCollector(const Config& config);
  StatsCollector(const StatsCollector&) = delete;
  StatsCollector(StatsCollector&&) = delete;
  ~StatsCollector() noexcept {}

  void configure(const daq::rc::TransitionCmd&) override;
  void prepareForRun(const daq::rc::TransitionCmd&) override;
  void stopRecording(const daq::rc::TransitionCmd&) override;
  void unconfigure(const daq::rc::TransitionCmd&) override;

  void updateFreeBuffers(boost::posix_time::ptime now, std::size_t n);
  void updateAssignments(boost::posix_time::ptime now, std::size_t n);
  void updateInputConnections(std::size_t n);

  void publishInput(const std::string& key, info::SFOngInput* info);

  void onEventCreation(boost::posix_time::ptime now, const Event& event);
  void onEventDestruction(boost::posix_time::ptime now, const Event& event,
      boost::posix_time::time_duration lifeTime);
  void onPtCreation(boost::posix_time::ptime now, const Event& event);
  void onPtDestruction(boost::posix_time::ptime now, const Event& event,
      boost::posix_time::time_duration lifeTime);
  void onPtExecution(boost::posix_time::ptime now, const Event& event,
      boost::posix_time::time_duration runTime);
  void onWtCreation(boost::posix_time::ptime now, const EventStream& stream);
  void onWtDestruction(boost::posix_time::ptime now, const EventStream& stream,
      boost::posix_time::time_duration lifeTime);
  void onWtExecution(boost::posix_time::ptime now, const EventStream& stream,
      boost::posix_time::time_duration runTime, bool success);

  void publishCounters(const std::string& key, info::SFOngCounters* info);

  Histograms& histograms() const
  {
    return *m_histograms;
  }

  uint32_t processedEvents() const
  {
    return m_ptCountersLive.at("Counters.Global").totalEvents;
  }

  uint32_t processedPhysicsEvents() const
  {
    return m_physicsEvents;
  }

private:

  const Config& m_config;

  std::unique_ptr<monsvc::PublishingController> m_publisher;

  monsvc::ptr<info::SFOngInput> m_inputInfo;

  tbb::concurrent_bounded_queue<std::pair<boost::posix_time::ptime, std::uint32_t>> m_assignments;
  tbb::concurrent_bounded_queue<std::pair<boost::posix_time::ptime, std::uint32_t>> m_freeBuffers;
  std::atomic<std::uint32_t> m_inputConnections;

  tbb::concurrent_unordered_map<std::string, CountersInfo> m_countersInfo;
  tbb::concurrent_unordered_map<std::string, ConcurrentCounters> m_eventsCountersLive;
  tbb::concurrent_unordered_map<std::string, ConcurrentTaskCounters> m_ptCountersLive;
  tbb::concurrent_unordered_map<std::string, ConcurrentTaskCounters> m_wtCountersLive;

  std::atomic<std::uint32_t> m_physicsEvents;

  std::unique_ptr<Histograms> m_histograms;

private:

  static void fillEventsInfo(info::SFOngCounters* info, const Counters& curr,
      boost::posix_time::time_duration interval);

  static void fillProcessingInfo(info::SFOngCounters* info, const TaskCounters& curr,
      boost::posix_time::time_duration interval);

  static void fillWritingInfo(info::SFOngCounters* info, const TaskCounters& curr,
      boost::posix_time::time_duration interval);

private:

};

inline void swap(StatsCollector::Counters& a, StatsCollector::Counters& b)
{
  using std::swap;
  swap(a.counts, b.counts);
  swap(a.lifeTimes, b.lifeTimes);
}

inline void swap(StatsCollector::TaskCounters& a, StatsCollector::TaskCounters& b)
{
  using std::swap;
  swap(a.counts, b.counts);
  swap(a.totalEvents, b.totalEvents);
  swap(a.totalSize, b.totalSize);
  swap(a.lifeTimes, b.lifeTimes);
  swap(a.runTimes, b.runTimes);
}

} // namespace SFOng

#endif // STATSCOLLECTOR_H
