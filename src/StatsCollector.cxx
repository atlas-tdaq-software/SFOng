#include "StatsCollector.h"
#include <algorithm>
#include <chrono>
#include <functional>
#include <numeric>
#include "Event.h"
#include "SFOng/dal/SFOngApplication.h"
#include "SFOng/info/SFOngCounters.h"
#include "SFOng/info/SFOngInput.h"
#include "eformat/StreamTag.h"
#include "ers/ers.h"
#include "monsvc/MonitoringService.h"
#include "src/Config.h"
#include "src/Histograms.h"
#include "src/Tag.h"
#include "utils/buffer.h"
namespace daq { namespace rc { class TransitionCmd; } }

namespace pt = boost::posix_time;

template<class Container>
static typename Container::value_type average(const Container& values)
{
  typedef typename Container::value_type Value;
  if (values.empty()) {
    return Value();
  }
  Value sum = std::accumulate(values.begin(), values.end(), Value());
  return sum / values.size();
}

template<class TimeValueContainer>
static typename TimeValueContainer::value_type::second_type timeWeightedAverage(const TimeValueContainer& pairs)
{
  typedef typename TimeValueContainer::const_iterator RandomAccessIterator;

  if (pairs.begin() == pairs.end()) {
    return 0;
  } else if ((pairs.begin() + 1) == pairs.end()) {
    return pairs.front().second;
  }
  // c_{i} = counts[i].second
  // t_{i} = counts[i].first
  // sum = \sum_{i=0}^{N-1} c_{i} * (t_{i+1} - t_{i})
  auto interval = pairs.back().first - pairs.front().first;
  if (interval.ticks() == 0) {
    return pairs.back().second;
  }
  pt::time_duration sum;
  for (RandomAccessIterator it = pairs.begin(); it + 1 != pairs.end(); ++it) {
    sum += ((it + 1)->first - it->first) * it->second;
  }
  return sum.ticks() / interval.ticks();
}

static monsvc::MonitoringService& monitoring()
{
  return monsvc::MonitoringService::instance();
}

namespace SFOng
{

StatsCollector::Counters::Counters()
{
}

StatsCollector::ConcurrentCounters::ConcurrentCounters() :
    count()
{
  // Limit the number of entries in the queues.
  // If for some reason they don't get emptied they won't consume too much memory.
  counts.set_capacity(10000);
  lifeTimes.set_capacity(10000);
}

void StatsCollector::ConcurrentCounters::fillCounters(Counters& c)
{
  std::pair<pt::ptime, uint32_t> count;
  while (counts.try_pop(count)) {
    c.counts.push_back(count);
  }
  // sort by timestamp
  std::sort(c.counts.begin(), c.counts.end());

  pt::time_duration time;
  while (lifeTimes.try_pop(time)) {
    c.lifeTimes.push_back(time);
  }
}

StatsCollector::TaskCounters::TaskCounters()
  : Counters(), totalEvents(0), totalSize(0), totalUncompressedSize(0)
  , lastTotalEvents(0), lastTotalSize(0), lastTotalUncompressedSize(0)
{
}

StatsCollector::ConcurrentTaskCounters::ConcurrentTaskCounters()
  : ConcurrentCounters(), totalEvents(), totalSize(), totalUncompressedSize()
  , lastTotalEvents(), lastTotalSize(), lastTotalUncompressedSize()
{
  runTimes.set_capacity(10000);
}

void StatsCollector::ConcurrentTaskCounters::fillCounters(TaskCounters& c)
{
  ConcurrentCounters::fillCounters(c);

  c.totalEvents = totalEvents;
  c.totalSize = totalSize;
  c.totalUncompressedSize = totalUncompressedSize;
  c.lastTotalEvents = lastTotalEvents;
  c.lastTotalSize = lastTotalSize;
  c.lastTotalUncompressedSize = lastTotalUncompressedSize;

  pt::time_duration time;
  while (runTimes.try_pop(time)) {
    c.runTimes.push_back(time);
  }

  lastTotalEvents.store(totalEvents.load());
  lastTotalSize.store(totalSize.load());
  lastTotalUncompressedSize.store(totalUncompressedSize.load());
}

StatsCollector::CountersInfo::CountersInfo() :
    once(new std::once_flag)
{
}

StatsCollector::StatsCollector(const Config& config) :
    Controllable(), m_config(config)
{
}

void StatsCollector::configure(const daq::rc::TransitionCmd&)
{
  m_publisher.reset(new monsvc::PublishingController(
      m_config.ipcPartition(), m_config.applicationName()));
  m_publisher->add_configuration_rules(
      m_config.database(), m_config.application());

  m_inputInfo = monitoring().register_object(
      "Input", new info::SFOngInput(), true,
      std::bind(&StatsCollector::publishInput, this, std::placeholders::_1, std::placeholders::_2));
  m_countersInfo["Counters.Global"].info = monitoring().register_object(
      "Counters.Global", new info::SFOngCounters(), true,
      std::bind(&StatsCollector::publishCounters, this, std::placeholders::_1, std::placeholders::_2));

  // Create the counter for the late events, as these are created
  // after the Event constructor
  auto key = "Counters." + m_config.lateEventsTag().fullName();
  m_countersInfo[key].info = monitoring().register_object(
      key, new info::SFOngCounters(), true,
      std::bind(&StatsCollector::publishCounters, this, std::placeholders::_1, std::placeholders::_2));

  m_publisher->start_publishing();
}

void StatsCollector::prepareForRun(const daq::rc::TransitionCmd&)
{
  // Remove the per-stream specific publishers and data-structures,
  // keep the global and late objects
  auto latekey = "Counters." + m_config.lateEventsTag().fullName();

  auto it = m_countersInfo.begin();
  while(it != m_countersInfo.end()) {
    if (it->first != "Counters.Global" && it->first != latekey) {
      monitoring().remove_object(it->second.info.get());
      it = m_countersInfo.unsafe_erase(it);
    } else {
      ++it;
    }
  }

  /* We cannot call clear on TBB::queues as there might be concurrent calls:
   * m_freeBuffers is push()-ed to in Input::publish for example, which in turn
   * is called by Igui button press, or periodically in RC thread since
   * "connected".
   * For m_assignment: it is at least being accessed (try_pop) by the monsvc
   * thread since configure().
   * At the same time, WritingManager::prepareForRun has not been called and
   * so push() rate should be minimal. So the queues are drained by consuming
   * what could be in it.
   */
  // DONT m_freeBuffers.clear(); DONT
  auto const deadline = std::chrono::steady_clock::now() +
      std::chrono::seconds(m_config.actionTimeout().seconds()) / 2;
  ERS_DEBUG(0, "clearing m_freeBuffers and m_assignments");
  std::pair<boost::posix_time::ptime, std::uint32_t> unused;
  while (m_freeBuffers.try_pop(unused)) {
    if (std::chrono::steady_clock::now() > deadline) {
      ERS_LOG("timeout clearing m_freeBuffers: monitoring data might be transiently inaccurate");
      break;
    }
  }

  // DONT m_assignments.clear(); DONT
  while (m_assignments.try_pop(unused)) {
    if (std::chrono::steady_clock::now() > deadline) {
      ERS_LOG("timeout clearing m_assignments: monitoring data might be transiently inaccurate");
      break;
    }
  }
  ERS_DEBUG(0, "cleared m_freeBuffers and m_assignments");

  m_eventsCountersLive.clear();
  m_ptCountersLive.clear();
  m_wtCountersLive.clear();

  // In case of stop -> start we need unregister histograms
  // before we re-created them
  m_histograms.reset();
  m_histograms.reset(new Histograms());

  m_physicsEvents = 0;
}

void StatsCollector::stopRecording(const daq::rc::TransitionCmd&)
{
  // Update the stats and run the EoE publication
  m_publisher->publish_all();

  // We do not unregister histograms here, as they are needed for SFOTZ
}

void StatsCollector::unconfigure(const daq::rc::TransitionCmd&)
{
  m_publisher->publish_all();
  m_publisher->stop_publishing();

  m_histograms.reset();

  monitoring().remove_object(m_inputInfo.get());
  for (auto& info: m_countersInfo) {
    monitoring().remove_object(info.second.info.get());
  }

  m_countersInfo.clear();

  m_publisher.reset();
}

void StatsCollector::updateFreeBuffers(pt::ptime now, std::size_t n)
{
  m_freeBuffers.try_push(std::make_pair(now, n));
}

void StatsCollector::updateAssignments(pt::ptime now, std::size_t n)
{
  m_assignments.try_push(std::make_pair(now, n));
}

void StatsCollector::updateInputConnections(std::size_t n)
{
  m_inputConnections = n;
}

void StatsCollector::publishInput(const std::string& /* key */, info::SFOngInput* info)
{
  std::pair<boost::posix_time::ptime, std::uint32_t> count;
  std::vector<decltype(count)> freeBuffers;
  while (m_freeBuffers.try_pop(count)) {
    freeBuffers.push_back(count);
  }
  info->FreeBuffersAverage = timeWeightedAverage(freeBuffers);

  std::vector<decltype(count)> assignments;
  while (m_assignments.try_pop(count)) {
    assignments.push_back(count);
  }
  info->WaitingEventsAverage = timeWeightedAverage(assignments);

  info->ActiveConnections = m_inputConnections;
}

void StatsCollector::onEventCreation(pt::ptime now, const Event& event)
{
  m_eventsCountersLive["Counters.Global"].counts.try_push(
      std::make_pair(now, ++m_eventsCountersLive["Counters.Global"].count));

  for (const auto& stream : event.rawStreams()) {
    auto key = "Counters." + stream.tag.fullName();
    // If this is the first time we see this stream tag, register a new Counters info with monsvc
    std::call_once(*m_countersInfo[key].once, [this, key] () {
      m_countersInfo[key].info = monitoring().register_object(
              key, new info::SFOngCounters(), true,
              std::bind(&StatsCollector::publishCounters, this, std::placeholders::_1, std::placeholders::_2));
    });
    m_eventsCountersLive[key].counts.try_push(
        std::make_pair(now, ++m_eventsCountersLive[key].count));
  }
}

void StatsCollector::onEventDestruction(pt::ptime now, const Event& event, pt::time_duration lifeTime)
{
  m_eventsCountersLive["Counters.Global"].counts.try_push(
      std::make_pair(now, --m_eventsCountersLive["Counters.Global"].count));
  m_eventsCountersLive["Counters.Global"].lifeTimes.try_push(lifeTime);

  for (const auto& stream : event.rawStreams()) {
    auto key = "Counters." + stream.tag.fullName();
    m_eventsCountersLive[key].counts.try_push(
        std::make_pair(now, --m_eventsCountersLive[key].count));
    m_eventsCountersLive[key].lifeTimes.try_push(lifeTime);
  }

  for (auto const & stream : event.fileStreams()) {
    for (auto const & otherStream : event.fileStreams()) {
      m_histograms->fillByStream(m_histograms->streamCorrelation_proc,
          stream.tag.fullName(), otherStream.tag.fullName());
    }
  }
}

void StatsCollector::onPtCreation(pt::ptime now, const Event& event)
{
  m_ptCountersLive["Counters.Global"].counts.try_push(
      std::make_pair(now, ++m_ptCountersLive["Counters.Global"].count));

  for (const auto& stream : event.rawStreams()) {
    auto key = "Counters." + stream.tag.fullName();
    m_ptCountersLive[key].counts.try_push(
        std::make_pair(now, ++m_ptCountersLive[key].count));
  }
}

void StatsCollector::onPtDestruction(pt::ptime now, const Event& event, pt::time_duration lifeTime)
{
  m_ptCountersLive["Counters.Global"].counts.try_push(
      std::make_pair(now, --m_ptCountersLive["Counters.Global"].count));
  m_ptCountersLive["Counters.Global"].lifeTimes.try_push(lifeTime);

  for (const auto& stream : event.rawStreams()) {
    auto key = "Counters." + stream.tag.fullName();
    m_ptCountersLive[key].counts.try_push(
        std::make_pair(now, --m_ptCountersLive[key].count));
    m_ptCountersLive[key].lifeTimes.try_push(lifeTime);
  }
}

void StatsCollector::onPtExecution(pt::ptime /* now */, const Event& event, pt::time_duration runTime)
{
  ++m_ptCountersLive["Counters.Global"].totalEvents;
  m_ptCountersLive["Counters.Global"].totalSize += event.rawSize();
  m_ptCountersLive["Counters.Global"].totalUncompressedSize += event.fullEventUncompressedSize();
  m_ptCountersLive["Counters.Global"].runTimes.try_push(runTime);

  bool physicsTagFound = false;
  for (const auto& stream : event.rawStreams()){
    auto const key = "Counters." + stream.tag.fullName();
    auto const data_size = utils::buffers_size(stream.iovector);

    ++m_ptCountersLive[key].totalEvents;
    m_ptCountersLive[key].totalSize += data_size;
    m_ptCountersLive[key].totalUncompressedSize += stream.uncompressedDataSize;
    m_ptCountersLive[key].runTimes.try_push(runTime);

    m_histograms->fillByStream(m_histograms->eventsPerStream_raw, stream.tag.fullName());
    m_histograms->fillByStream(m_histograms->sizePerStream_raw, stream.tag.fullName(), data_size);
    for (const auto& otherStream : event.rawStreams()) {
      m_histograms->fillByStream(m_histograms->streamCorrelation_raw,
          stream.tag.fullName(), otherStream.tag.fullName());
    }

    // Exclusive count of physics and express events
    if (!physicsTagFound) {
      eformat::TagType type = eformat::helper::string_to_tagtype(stream.tag.type);
      if (type == eformat::PHYSICS_TAG || type == eformat::EXPRESS_TAG) {
        ++m_physicsEvents;
        physicsTagFound = true;
      }
    }
  }
}

void StatsCollector::onWtCreation(pt::ptime now, const EventStream& stream)
{
  m_wtCountersLive["Counters.Global"].counts.try_push(
      std::make_pair(now, ++m_wtCountersLive["Counters.Global"].count));

  auto key = "Counters." + stream.tag.fullName();
  m_wtCountersLive[key].counts.try_push(
      std::make_pair(now, ++m_wtCountersLive[key].count));
}

void StatsCollector::onWtDestruction(pt::ptime now, const EventStream& stream,
    pt::time_duration lifeTime)
{
  m_wtCountersLive["Counters.Global"].counts.try_push(
      std::make_pair(now, --m_wtCountersLive["Counters.Global"].count));
  m_wtCountersLive["Counters.Global"].lifeTimes.try_push(lifeTime);

  auto key = "Counters." + stream.tag.fullName();
  m_wtCountersLive[key].counts.try_push(
      std::make_pair(now, --m_wtCountersLive[key].count));
  m_wtCountersLive[key].lifeTimes.try_push(lifeTime);
}

void StatsCollector::onWtExecution(pt::ptime /* now */, const EventStream& stream,
    pt::time_duration runTime, bool success)
{
  std::uint32_t const size = utils::buffers_size(stream.iovector);

  if (success) {
    ++m_wtCountersLive["Counters.Global"].totalEvents;
    m_wtCountersLive["Counters.Global"].totalSize += size;
    m_wtCountersLive["Counters.Global"].totalUncompressedSize += stream.uncompressedDataSize;
  }
  m_wtCountersLive["Counters.Global"].runTimes.try_push(runTime);

  auto const key = "Counters." + stream.tag.fullName();
  if (success) {
    ++m_wtCountersLive[key].totalEvents;
    m_wtCountersLive[key].totalSize += size;
    m_wtCountersLive[key].totalUncompressedSize += stream.uncompressedDataSize;
  }
  m_wtCountersLive[key].runTimes.try_push(runTime);

  if (success) {
    m_histograms->fillByStream(m_histograms->eventsPerStream_proc, stream.tag.fullName());
    m_histograms->fillByStream(m_histograms->sizePerStream_proc, stream.tag.fullName(), size);
  }
}

void StatsCollector::publishCounters(const std::string& key, info::SFOngCounters* info)
{
  auto now = pt::microsec_clock::universal_time();
  pt::time_duration interval = now - m_countersInfo[key].timestamp;

  // Make sure we do not divide by 0 (or insignificantly small times anyway)
  if (interval.total_milliseconds() == 0) {
    ERS_LOG("Received two monsvc callbacks in less than 1 ms!");
    return;
  }

  m_countersInfo[key].timestamp = now;

  Counters current_events_counters;
  m_eventsCountersLive[key].fillCounters(current_events_counters);
  fillEventsInfo(info, current_events_counters, interval);

  TaskCounters current_pt_counters;
  m_ptCountersLive[key].fillCounters(current_pt_counters);
  fillProcessingInfo(info, current_pt_counters, interval);

  TaskCounters current_wt_counters;
  m_wtCountersLive[key].fillCounters(current_wt_counters);
  fillWritingInfo(info, current_wt_counters, interval);
}

void StatsCollector::fillEventsInfo(
      info::SFOngCounters* info
    , const Counters& curr
    , pt::time_duration)
{
  info->EventsInsideAverage = timeWeightedAverage(curr.counts);
  info->EventsLifeTimeAverage = average(curr.lifeTimes).total_microseconds();
}

void StatsCollector::fillProcessingInfo(
      info::SFOngCounters* info
    , const TaskCounters& curr
    , pt::time_duration interval)
{
  info->ProcessingTasksAverage = timeWeightedAverage(curr.counts);
  info->ProcessingLifeTimeAverage = average(curr.lifeTimes).total_microseconds();
  info->ProcessingRunTimeAverage = average(curr.runTimes).total_microseconds();
  info->ProcessingTotalEvents = curr.totalEvents;
  info->ProcessingEventRate = 1000000 * static_cast<float>(curr.totalEvents - curr.lastTotalEvents)
      / interval.total_microseconds();
  info->ProcessingTotalData = curr.totalSize;
  info->ProcessingDataRate = 1000000 * static_cast<float>(curr.totalSize - curr.lastTotalSize)
      / interval.total_microseconds();
  info->ProcessingTotalUncompressedData = curr.totalUncompressedSize;
  info->ProcessingUncompressedDataRate = 1000000 *
      static_cast<float>(curr.totalUncompressedSize - curr.lastTotalUncompressedSize)
      / interval.total_microseconds();
}

void StatsCollector::fillWritingInfo(
      info::SFOngCounters* info
    , const TaskCounters& curr
    , pt::time_duration interval)
{
  info->WritingTasksAverage = timeWeightedAverage(curr.counts);
  info->WritingLifeTimeAverage = average(curr.lifeTimes).total_microseconds();
  info->WritingRunTimeAverage = average(curr.runTimes).total_microseconds();
  info->WritingTotalEvents = curr.totalEvents;
  info->WritingEventRate = 1000000 * static_cast<float>(curr.totalEvents - curr.lastTotalEvents)
      / interval.total_microseconds();
  info->WritingTotalData = curr.totalSize;
  info->WritingDataRate = 1000000 * static_cast<float>(curr.totalSize - curr.lastTotalSize)
      / interval.total_microseconds();
  info->WritingTotalUncompressedData = curr.totalUncompressedSize;
  info->WritingUncompressedDataRate = 1000000 *
      static_cast<float>(curr.totalUncompressedSize - curr.lastTotalUncompressedSize)
      / interval.total_microseconds();
}

} // namespace SFOng
