#include "Cleaner.h"
#include "Config.h"
#include "StatsCollector.h"
#include "SfotzPublisher.h"
#include "WritingManager.h"
#include "Input.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/FSM/FSMStates.h"

namespace SFOng {

Cleaner::Cleaner(Config& config, StatsCollector& sc, SfotzPublisher& sp,
    WritingManager& wm, Input& input)
  : m_config(config)
  , m_statsCollector(sc)
  , m_sfotzPublisher(sp)
  , m_writingManager(wm)
  , m_input(input)
{
  //noop
}

void Cleaner::onExit(daq::rc::FSM_STATE fsm_state) noexcept
{
  // 1- trigger stopRecording to close files and sync SFO/T0 DB
  // 2- trigger unconfigure to stop and joint threads

  if (m_input.isRunning()) {
    ERS_LOG("forcing StopRecording transition");
    //Here we need to protect ourselves in case we are stopping already
    //see the stop method in the Input
    daq::rc::TransitionCmd cmd(daq::rc::FSM_COMMAND::STOP);
    m_input.stopRecording(cmd);
    m_writingManager.stopRecording(cmd);
    m_statsCollector.stopRecording(cmd);
    m_sfotzPublisher.stopRecording(cmd);
    m_config.stopRecording(cmd);
    ERS_LOG("StopRecording transition completed");
  }

  if (fsm_state > daq::rc::FSM_STATE::INITIAL) {
    ERS_LOG("forcing unconfigure transition");
    daq::rc::TransitionCmd cmd(daq::rc::FSM_COMMAND::UNCONFIGURE);
    m_input.unconfigure(cmd);
    m_writingManager.unconfigure(cmd);
    m_statsCollector.unconfigure(cmd);
    m_sfotzPublisher.unconfigure(cmd);
    m_config.unconfigure(cmd);
    ERS_LOG("unconfigure transition completed");
  }
}

} // namespace SFOng