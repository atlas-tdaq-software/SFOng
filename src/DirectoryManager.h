#ifndef SFONG_SRC_DIRECTORYMANAGER_H_
#define SFONG_SRC_DIRECTORYMANAGER_H_

/* The purpose of this class is to keep track of files being written
 * by directories between their creation by EventStorage::DataWriter
 * and their publication to SFO T0 handshake database (SFOTZ).
 *
 * This is required for a proper implementation of directory locking.
 * Unlocking of a directory can only be done once all files have been
 * published to SFOTZ. Otherwise, i.e. the directory is unlocked as soon
 * as all files are closed, an external software could (and actually
 * does rarely) handle a file and check its status in SFOTZ before it
 * actually exists in the DB, leading to errors.
 *
 * So here, thanks to callback mechanism from EventStorage::DataWriter and
 * SFOTZ::SFOTZThreads, we keep track of existing files not yet published
 * in SFOTZ. When the WritingManager wants to unlock a path, it waits for
 * this object to report no files remain unpublished.
 */

#include <cstdint>
#include <map>
#include <mutex>
#include <set>
#include <string>
#include "EventStorage/DataWriterCallBack.h"
#include "SFOTZ/DBUpdateInfo.h"
#include "SFOTZ/SFOTZCallback.h"

class DirectoryManager
    : public SFOTZ::SFOTZCallback
    , public EventStorage::DataWriterCallBack
{
public:
  DirectoryManager();
  virtual ~DirectoryManager();

  //SFOTZCallback interface
  void db_updated(SFOTZ::DBInfoPointer update_info) override;

  //DataWriterCallBack interface
  void FileWasOpened(std::string logicalfileName, std::string fileName, std::string streamtype,
      std::string streamname, std::string sfoid, std::string guid, unsigned int runNumber,
      unsigned int filenumber, unsigned int lumiblock) override;

  void FileWasClosed(std::string, std::string, std::string, std::string, std::string,
      std::string, std::string, unsigned int, unsigned int, unsigned int,
      unsigned int, uint64_t) override;

  /* returns: true when the directory has no longer any associated file,
   *          false on timeout
   * timeout has a resolution of 100 ms
   */
  bool wait_for_empty_dir(std::string const & dir, unsigned timeout_ms);

  /* after a timeout, user object probably wants to clean any residual entries
   */
  void clear_dir_entries(std::string const & dir);

private:
  typedef std::map<std::string, std::set<std::string>> TDirectoryFiles;
  TDirectoryFiles dir_files;
  std::mutex dir_files_mutex;
};

#endif /* SFONG_SRC_DIRECTORYMANAGER_H_ */
