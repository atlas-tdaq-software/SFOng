#include "WeightedIOService.h"
#include <deque>
#include <functional>
#include <map>
#include <ostream>
#include <vector>
#include "SFOng/info/SFOngWeightedIOService.h"
#include "ers/ers.h"
#include "monsvc/MonitoringService.h"
#include "src/SlidingWindowStats.h"
#include "src/Tag.h"

namespace
{
  int ID = 0;
} // anonymous namespace

namespace SFOng
{

WeightedIOService::WeightedIOService(
      std::size_t concurrency_hint
    , boost::posix_time::time_duration const & weight_period
    , TWatchedStreams const & watched_streams)
  : m_ioService(concurrency_hint)
  , m_weightData(weight_period)
  , m_watchedStreams(watched_streams)
{
  // Create a publish object in monsvc lib
  std::ostringstream obj_name;
  obj_name << "WIOS_" << ID++;
  static const bool LIB_OWNS_OBJECT = true;

  m_info = monsvc::MonitoringService::instance().register_object(
      obj_name.str(), new info::SFOngWeightedIOService, LIB_OWNS_OBJECT,
      std::bind(&WeightedIOService::publishInfo_nolock, this,
          std::placeholders::_1, std::placeholders::_2));
}

WeightedIOService::~WeightedIOService()
{
  monsvc::MonitoringService::instance().remove_object(m_info.get());
}

void WeightedIOService::registerTag(Tag const & tag)
{
  // Lock because we are modifying information used by publication.
  // This method can block (while publishInfo_nolock is called by monsvc).

  monsvc::ptr<info::SFOngWeightedIOService>::scoped_lock lock(m_info);
  checkWatchedStreams(tag);
  m_associatedStreams.insert(tag);
}

void WeightedIOService::unregisterTag(Tag const & t)
{
  // Lock because we are modifying information used by publication.
  // This method can block (while publishInfo_nolock is called by monsvc).

  monsvc::ptr<info::SFOngWeightedIOService>::scoped_lock lock(m_info);
  m_associatedStreams.erase(t);
}

void WeightedIOService::publishInfo_nolock(
      const std::string& //obj_name
    , info::SFOngWeightedIOService* info
    )
{
  // monsvc is holding the lock associated with m_info: noone is publishing at
  // the same time. Be as quick as possible here, because un/registerTag calls
  // can be blocked while we're here.

  info->Weight = this->weight();
  info->AssociatedStreams.clear();
  info->AssociatedStreams.reserve(m_associatedStreams.size());
  std::ostringstream tag_name;

  for (Tag const & tag : m_associatedStreams) {
    tag_name.str(std::string());
    tag_name << tag;
    info->AssociatedStreams.push_back(tag_name.str());
  }
}

uint64_t WeightedIOService::weight() const
{
  return m_weightData.sum();
}

void WeightedIOService::addWeight(boost::posix_time::ptime const & ts, uint64_t value)
{
  m_weightData.push(ts, value);
}

void WeightedIOService::checkWatchedStreams(Tag const & tag) const
{
  TWatchedStreams::const_iterator const it = m_watchedStreams.find(tag);
  if (it == m_watchedStreams.end())
    return;

  // it->second contains the streams that cannot be associated with "tag"

  for (Tag const & as: m_associatedStreams) {
    if (it->second.find(as) != it->second.end()) {
      if (tag.lumiblock == as.lumiblock) {
        std::ostringstream problematic_streams;
        problematic_streams << tag << " and " << as
            << " are handled by the same thread: possible under-performance";
        ers::warning(AssignPolicyFailure(ERS_HERE, problematic_streams.str()));
      }
      //else: if it's not the same lumiblock, it's probably less of a problem
    }
  }
}

} // SFOng namespace

std::ostream & operator<< (std::ostream & os, SFOng::WeightedIOService const & wios)
{
  os << std::hex << &wios << std::dec << " (weight=" << wios.weight() << ")";
  return os;
}
