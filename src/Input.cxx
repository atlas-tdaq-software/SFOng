#include "Input.h"
#include <exception>
#include <algorithm>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/system/error_code.hpp>
#include <functional>
#include <sstream>
#include <string>
#include "Config.h"
#include "Event.h"
#include "EventStorage/EventStorageRecords.h"
#include "StatsCollector.h"
#include "Types.h"
#include "WritingManager.h"
#include "asyncmsg/Error.h"
#include "asyncmsg/Message.h"
#include "asyncmsg/NameService.h"
#include "asyncmsg/Server.h"
#include "asyncmsg/Session.h"
#include "ers/ers.h"
#include "utils/unique_ptr_utils.h"
namespace daq { namespace rc { class TransitionCmd; } }
namespace ers { class Context; }

namespace pt = boost::posix_time;

ERS_DECLARE_ISSUE(SFOng,
    SystemError,
    message << " " << "(" << category << ": " << number << ")",
    ((std::string) message) ((std::string) category) ((int) number))

ERS_DECLARE_ISSUE(SFOng,
    InputServerError,
    "Input server problem " << action,
    ((std::string) action))

ERS_DECLARE_ISSUE(SFOng,
    InputWatchdogError,
    "Input watchdog timer failure",
    ERS_EMPTY)

ERS_DECLARE_ISSUE(SFOng,
    InputTransferError,
    "Could not transfer event data",
    ERS_EMPTY)

ERS_DECLARE_ISSUE(SFOng,
    InputSessionError,
    "Problem communicating with " << remoteName << " (" << remoteEndpoint << ") while " << action,
    ((std::string) remoteName) ((std::string) remoteEndpoint) ((std::string) action))

ERS_DECLARE_ISSUE(SFOng,
    UnexpectedMessageType,
    "Unexpected message type 0x" << std::hex << type,
    ((std::uint32_t) type))

ERS_DECLARE_ISSUE(SFOng,
    InvalidMessageSize,
    "Invalid size " << size << " for message type 0x" << std::hex << type,
    ((std::uint32_t) type) ((std::uint32_t) size))

ERS_DECLARE_ISSUE(SFOng,
    TransferTimeout,
    "Transfer message did not arrive within the deadline",
    ERS_EMPTY)

namespace
{

SFOng::SystemError makeSystemError(const ers::Context& context,
    const boost::system::error_code& error)
{
  return SFOng::SystemError(context, error.message(), error.category().name(), error.value());
}

}

namespace SFOng
{

class Input::Session: public daq::asyncmsg::Session
{

private:

  struct AssignMessage: public daq::asyncmsg::InputMessage
  {

    static const std::uint32_t TYPE_ID = 0x00DCDF40;

    virtual std::uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual std::uint32_t transactionId() const
    {
      return 0;
    }

    virtual void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers)
    {
      // FIXME: this assumes we are on a little-endian arch
      buffers.emplace_back(&nAssignedEvents, sizeof(nAssignedEvents));
    }

    std::uint32_t nAssignedEvents;

  };

  struct UpdateMessage: public daq::asyncmsg::OutputMessage
  {

    static const std::uint32_t TYPE_ID = 0x00DCDF41;

    virtual uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual uint32_t transactionId() const
    {
      return 0;
    }

    virtual void toBuffers(std::vector<boost::asio::const_buffer>& buffers) const
    {
      buffers.emplace_back(&nRequestedEvents, sizeof(nRequestedEvents));
      buffers.emplace_back(acknowledgedGIds.data(), acknowledgedGIds.size() * sizeof(std::uint64_t));
    }

    std::uint32_t nRequestedEvents;
    std::vector<std::uint64_t> acknowledgedGIds;

  };

  struct TransferMessage: public daq::asyncmsg::InputMessage
  {

    static const std::uint32_t TYPE_ID = 0x00DCDF42;

    virtual uint32_t typeId() const
    {
      return TYPE_ID;
    }

    virtual uint32_t transactionId() const
    {
      return 0;
    }

    virtual void toBuffers(std::vector<boost::asio::mutable_buffer>& buffers)
    {
      buffers.emplace_back(&gid, sizeof(gid));
      buffers.emplace_back(buffer->data(), buffer->size());
    }

    std::uint64_t gid;
    BufferHandle buffer;

  };

public:

  Session(Input& input, boost::asio::io_service& ioService) :
      daq::asyncmsg::Session(ioService),
      m_input(input),
      m_nNeededBuffers(0)
  {
  }

private:

  void onOpen() noexcept override
  {
    ERS_LOG("Connection to " << remoteName() << " open (session_ptr: "
        << std::hex << this << std::dec << ")");
    asyncReceive();
  }

  void onOpenError(const boost::system::error_code& error) noexcept override
  {
    abort(ERS_HERE, "opening session", makeSystemError(ERS_HERE, error));
  }

  void onClose() noexcept override
  {
    ERS_LOG("Connection to " << remoteName() << " closed (session_ptr: "
        << std::hex << this << std::dec << ")");
    m_input.onSessionClose(std::dynamic_pointer_cast<Session>(shared_from_this()));
  }

  void onCloseError(const boost::system::error_code& error) noexcept override
  {
    // Ignore errors caused by calling asyncClose() twice
    if (error != daq::asyncmsg::Error::SESSION_NOT_OPEN) {
      std::ostringstream iss;
      iss << remoteEndpoint();
      ers::error(InputSessionError(ERS_HERE, remoteName(), iss.str(), "closing connection",
          makeSystemError(ERS_HERE, error)));
      onClose();
    }
  }

public:

  void asyncTransfer(BufferHandle buffer)
  {
    ERS_ASSERT(state() == Session::State::OPEN);
    ERS_ASSERT(m_nNeededBuffers > 0);
    --m_nNeededBuffers;
    auto self = shared_from_this();
    getStrand().dispatch([this, self, buffer] () {
      m_buffers.push_back(buffer);
      auto update = utils::make_unique<UpdateMessage>();
      update->nRequestedEvents = 1;
      asyncSend(std::move(update));
    });
  }

private:

  void onSend(std::unique_ptr<const daq::asyncmsg::OutputMessage> message) noexcept override
  {
    auto update = utils::static_pointer_cast<const UpdateMessage>(std::move(message));
    for (std::uint32_t i = 0; i < update->nRequestedEvents; ++i) {
      m_deadlines.push_back(std::chrono::steady_clock::now() +
          m_input.m_config.eventTransferTimeout());
    }
  }

  void onSendError(const boost::system::error_code& error,
      std::unique_ptr<const daq::asyncmsg::OutputMessage> /* message */) noexcept override
  {
    abort(ERS_HERE, "sending Update message", makeSystemError(ERS_HERE, error));
  }

  std::unique_ptr<daq::asyncmsg::InputMessage> createMessage(std::uint32_t typeId,
      std::uint32_t /* transactionId */, std::uint32_t size) noexcept override
  {
    if (typeId == AssignMessage::TYPE_ID) {
      if (size != sizeof(std::uint32_t)) {
        abort(ERS_HERE, "receiving Assign message", InvalidMessageSize(ERS_HERE, typeId, size));
        return std::unique_ptr<daq::asyncmsg::InputMessage>();
      }
      return utils::make_unique<AssignMessage>();
    }
    else if (typeId == TransferMessage::TYPE_ID) {
      if (size <= sizeof(std::uint64_t)) {
        abort(ERS_HERE, "receiving Transfer message", InvalidMessageSize(ERS_HERE, typeId, size));
        return std::unique_ptr<daq::asyncmsg::InputMessage>();
      }
      if (m_buffers.empty()) {
        abort(ERS_HERE, "receiving Transfer message", UnexpectedMessageType(ERS_HERE, typeId));
        return std::unique_ptr<daq::asyncmsg::InputMessage>();
      }
      auto transfer = utils::make_unique<TransferMessage>();
      transfer->buffer = std::move(m_buffers.front());
      m_buffers.pop_front();
      transfer->buffer->resize(size - sizeof(std::uint64_t));
      return transfer;
    }
    else {
      abort(ERS_HERE, "receiving unknown message", UnexpectedMessageType(ERS_HERE, typeId));
      return std::unique_ptr<daq::asyncmsg::InputMessage>();
    }
  }

  void onReceive(std::unique_ptr<daq::asyncmsg::InputMessage> message) noexcept override
  {
    if (message->typeId() == AssignMessage::TYPE_ID) {
      auto assign = utils::static_pointer_cast<AssignMessage>(std::move(message));
      m_nNeededBuffers += assign->nAssignedEvents;
      asyncReceive();
      m_input.distributeBuffers();
    }
    else if (message->typeId() == TransferMessage::TYPE_ID) {
      m_deadlines.pop_front();
      auto transfer = utils::static_pointer_cast<TransferMessage>(std::move(message));
      EventHandle event;
      try {
        event.reset(new Event(m_input.m_statsCollector, m_input,
                              transfer->gid,
                              m_input.m_config.runParams().run_number,
                              transfer->buffer,
                              m_input.m_config.lumiblockEnabled()));
      }
      catch (InvalidEvent& e) {
        std::ostringstream iss;
        iss << remoteEndpoint();
        ers::warning(InputSessionError(ERS_HERE, remoteName(), iss.str(),
            "parsing event data (data will be saved to the debug stream)", e));
        event.reset(new Event(m_input.m_statsCollector, m_input, transfer->gid, transfer->buffer,
            m_input.m_config.corruptEventsTag()));
      }
      auto update = utils::make_unique<UpdateMessage>();
      update->acknowledgedGIds.push_back(transfer->gid);
      asyncSend(std::move(update));
      asyncReceive();
      m_input.onSessionTransfer(event);
    }
  }

  void onReceiveError(const boost::system::error_code& error,
      std::unique_ptr<daq::asyncmsg::InputMessage> message) noexcept override
  {
    if (message && message->typeId() == AssignMessage::TYPE_ID) {
      abort(ERS_HERE, "receiving Assign message", makeSystemError(ERS_HERE, error));
    }
    else if (message && message->typeId() == TransferMessage::TYPE_ID) {
      auto transfer = utils::static_pointer_cast<TransferMessage>(std::move(message));
      m_buffers.push_front(std::move(transfer->buffer));
      abort(ERS_HERE, "receiving Transfer message", makeSystemError(ERS_HERE, error));
    }
    else {
      // Don't complain if the connection is closed by the client and that is expected
      if (error == boost::asio::error::eof && !m_input.m_running &&
          m_nNeededBuffers == 0 && m_buffers.empty()) {
        asyncClose();
      }
      else {
        abort(ERS_HERE, "receiving unknown message", makeSystemError(ERS_HERE, error));
      }
    }
  }

private:

  void abort(const ers::Context& context, const std::string& action, const std::exception& cause)
  {
    std::ostringstream iss;
    iss << remoteEndpoint();
    iss << " (session_ptr: " << std::hex << this << std::dec << ")";
    auto issue = InputSessionError(context, remoteName(), iss.str(), action, cause);

    ers::error(issue);

    // Reset
    if (state() == State::OPEN) {
      asyncClose();
    }
    m_nNeededBuffers = 0;
    m_deadlines.clear();
    decltype(m_buffers) buffers;
    std::swap(m_buffers, buffers);
    for (auto& buffer : buffers) {
      m_input.onSessionTransferError(issue, std::move(buffer));
    }
  }

public:

  std::uint32_t nNeededBuffers() const
  {
    return m_nNeededBuffers;
  }

  void checkTimeouts(const std::chrono::steady_clock::time_point& now)
  {
    auto self = shared_from_this();
    getStrand().post([this, self, now] () {
      if (state() == State::OPEN && !m_deadlines.empty() && now > m_deadlines.front()) {
        abort(ERS_HERE, "waiting for Transfer message", TransferTimeout(ERS_HERE));
      }
    });
  }

private:

  Input& m_input;

  std::deque<BufferHandle> m_buffers;
  std::deque<std::chrono::steady_clock::time_point> m_deadlines;
  std::atomic<std::uint32_t> m_nNeededBuffers;

};

class Input::Server: public daq::asyncmsg::Server
{

public:

  Server(Input& input, boost::asio::io_service& ioService) :
      daq::asyncmsg::Server(ioService),
      m_input(input)
  {
  }

private:

  virtual void onAccept(std::shared_ptr<daq::asyncmsg::Session> session) noexcept override
  {
    m_input.onServerAccept(std::static_pointer_cast<Session>(session));
  }

  virtual void onAcceptError(const boost::system::error_code& error,
      std::shared_ptr<daq::asyncmsg::Session> session) noexcept override
  {
    m_input.onServerAcceptError(error, std::static_pointer_cast<Session>(session));
  }

private:

  Input& m_input;

};

Input::Input(const Config& config, WritingManager& wm, StatsCollector& sc) :
    m_config(config),
    m_writingManager(wm),
    m_statsCollector(sc),
    m_running(false)
{
}

void Input::configure(const daq::rc::TransitionCmd&)
{
  // Allocate buffers
  m_freeBuffers.reserve(m_config.nBuffers());
  for (std::uint32_t i = 0; i < m_config.nBuffers(); ++i) {
    m_freeBuffers.emplace_back(new Buffer(m_config.bufferSize_KiB() * 1024));
  }
  auto now = boost::posix_time::microsec_clock::universal_time();
  m_statsCollector.updateFreeBuffers(now, m_freeBuffers.size());

  for (std::uint32_t i = 0; i < m_config.nInputThreads(); ++i) {
    // Create io_service
    m_ioServices.emplace_back(1);
    auto& ioService = m_ioServices.back();
    // Prevent io_service::run() from returning when there are no pending asynchronous operations
    m_ioServiceWorks.emplace_back(ioService);
    // Run io_service
    m_ioServiceThreads.emplace_back([&] () {
        ioService.run();
      });
  }
  m_nextIoService = m_ioServices.begin();

  // Create server
  m_server.reset(new Server(*this, m_ioServices.front()));
  // Start listening for incoming connections
  m_server->listen(m_config.applicationName());
  ERS_LOG("Input server listening on endpoint: " << m_server->localEndpoint());

  // Publish server endpoint via asyncmsg::NameService
  daq::asyncmsg::NameService ns(m_config.ipcPartition(), m_config.dataNetworks());
  try {
    ns.publish(m_config.applicationName(), m_server->localEndpoint().port());
  } catch (daq::asyncmsg::CannotPublish& e) {
    throw InputServerError(ERS_HERE, "publishing endpoint on NameService", e);
  } catch (std::exception &e) {
    throw InputServerError(ERS_HERE, "publishing endpoint on NameService", e);
  }

  // Create watchdog timer
  m_timer.reset(new boost::asio::steady_timer(m_ioServices.front()));
  // Start watchdog timer
  m_timer->expires_at(std::chrono::steady_clock::now());
  m_timer->async_wait(std::bind(&Input::onTimer, this, std::placeholders::_1));

  // Accept the next incoming connection
  auto newSession = std::make_shared<Session>(*this, *m_nextIoService);
  if (++m_nextIoService == m_ioServices.end()) {
    m_nextIoService = m_ioServices.begin();
  }
  m_server->asyncAccept(newSession);
}

void Input::prepareForRun(const daq::rc::TransitionCmd&)
{
  m_running = true;
}

void Input::stopRecording(const daq::rc::TransitionCmd&)
{
  StopMutex::getMutex().lock();

  m_running = false;

  // Wait for all buffers to be returned
  m_freeBuffersMutex.lock();
  while (m_freeBuffers.size() < m_config.nBuffers()) {
    m_freeBuffersMutex.unlock();
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    m_freeBuffersMutex.lock();
  }
  m_freeBuffersMutex.unlock();
}

void Input::unconfigure(const daq::rc::TransitionCmd&)
{
  StopMutex::getMutex().lock();

  // Stop listening and accepting connections
  m_server->close();

  // Stop watchdog timer
  m_timer->cancel();


  /* Note: DCMs close connection at disconnect transition so, here, all
   * connections should be closed already. asyncClose is still called below in
   * case of physical TPU failure which leaves connection open.
   */

  {
    auto const start_ts = pt::second_clock::universal_time();
    std::lock_guard<std::mutex> lock_sessions(m_sessionsMutex);

    // Close all connections
    for (auto& session : m_sessions) {
      session->asyncClose();
    }

    // Wait for sessions to terminate
    while (!m_sessions.empty()) {
      // release lock to process session closures
      m_sessionsMutex.unlock();
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      m_sessionsMutex.lock();

      // timeout: see ADHI-4913
      if ((pt::second_clock::universal_time() - start_ts) > m_config.actionTimeout()) {
        std::ostringstream oss;
        oss << "timeout waiting for session close; remaining sessions:";
        for (auto const & s : m_sessions) {
          oss << " " << s->remoteName() << " (" << s->remoteEndpoint() << ", "
              << static_cast<int>(s->state()) << ")";
        }
        ERS_LOG(oss.str());
        // Force sessions clear
        m_sessions.clear();
      }
    }
  } // unlock sessions

  // Allow io_service::run() to return when there are no pending asynchronous operations
  m_ioServiceWorks.clear();
  // Wait for io_service::run() to return (i.e. wait for all asynchronous operations to finish)
  for (auto& thread : m_ioServiceThreads) {
    thread.join();
  }
  m_ioServiceThreads.clear();

  // Destroy server
  m_server.reset();

  // Destroy watchdog timer
  m_timer.reset();

  // Destroy io_services.
  m_ioServices.clear();

  // Destroy buffers
  m_freeBuffers.clear();
  auto now = boost::posix_time::microsec_clock::universal_time();
  m_statsCollector.updateFreeBuffers(now, m_freeBuffers.size());
}

void Input::returnBuffer(BufferHandle buffer)
{
  distributeBuffers(std::move(buffer));
}

void Input::onServerAccept(std::shared_ptr<Session> session)
{
  {
    std::lock_guard<std::mutex> lock(m_sessionsMutex);
    m_sessions.emplace_back(session);
    m_statsCollector.updateInputConnections(m_sessions.size());
  }

  // Accept the next incoming connection
  auto newSession = std::make_shared<Session>(*this, *m_nextIoService);
  if (++m_nextIoService == m_ioServices.end()) {
    m_nextIoService = m_ioServices.begin();
  }
  m_server->asyncAccept(newSession);
}

void Input::onServerAcceptError(const boost::system::error_code& error,
    std::shared_ptr<Session> session)
{
  if (error == boost::asio::error::operation_aborted) {
    return;
  }
  else if (error) {
    ers::warning(InputServerError(ERS_HERE, "accepting incoming connection",
        makeSystemError(ERS_HERE, error)));
  }

  session.reset();

  // Accept the next incoming connection
  auto newSession = std::make_shared<Session>(*this, *m_nextIoService);
  if (++m_nextIoService == m_ioServices.end()) {
    m_nextIoService = m_ioServices.begin();
  }
  m_server->asyncAccept(newSession);
}

void Input::onSessionTransfer(EventHandle event)
{
  ERS_DEBUG(3, "Input: retrieved event " << event);

  pt::ptime timestamp = pt::microsec_clock::universal_time();

  m_statsCollector.onPtCreation(timestamp, *event);

  if (m_config.writingEnabled()) {
    m_writingManager.addEvent(event);
  }

  auto now = pt::microsec_clock::universal_time();
  auto runTime = now - timestamp;
  /* Runtime (given by onPtExecution) and lifetime (given by onPtDestruction)
   * are the same with this implementation. It makes sense because
   * ProcessingTasks do not do processing anymore.
   * So why do we keep this monitoring information? For historical reasons,
   * because it might change in the future and for consistency reasons (with
   * WritingTasks).
   * I really tried to convince Wainer to remove it but he eventually convinced
   * me to keep (and I left his office with extra work, so be acreful next time
   * you try).
   */
  m_statsCollector.onPtExecution(now, *event, runTime);
  m_statsCollector.onPtDestruction(now, *event, runTime);
}

void Input::onSessionTransferError(const ers::Issue& error,
    BufferHandle buffer)
{
  ers::warning(InputTransferError(ERS_HERE, error));
  returnBuffer(std::move(buffer));
}

void Input::onSessionClose(std::shared_ptr<Session> session)
{
  std::lock_guard<std::mutex> lock(m_sessionsMutex);
  auto const new_end = std::remove(m_sessions.begin(), m_sessions.end(), session);
  if (new_end == m_sessions.end()) {
    ERS_LOG("closing session already removed from Input: session_ptr: "
        << std::hex << session.get() << std::dec);
  } else {
    m_sessions.erase(new_end, m_sessions.end());
  }
  m_statsCollector.updateInputConnections(m_sessions.size());
}

void Input::distributeBuffers(BufferHandle returnedBuffer)
{
  if (returnedBuffer == nullptr && (!m_running || m_writingManager.isXoff())) {
    return;
  }

  std::lock_guard<std::mutex> freeBuffersLock(m_freeBuffersMutex);

  if (returnedBuffer != nullptr) {
    m_freeBuffers.push_back(std::move(returnedBuffer));
  }
  else if (m_freeBuffers.empty()) {
    return;
  }

  std::lock_guard<std::mutex> sessionsLock(m_sessionsMutex);

  std::vector<Session*> selectedSessions;
  std::uint32_t nNeededBuffers = 0;
  for (auto& session : m_sessions) {
    if (session->nNeededBuffers() != 0) {
      nNeededBuffers += session->nNeededBuffers();
      selectedSessions.push_back(session.get());
    }
  }
  std::random_shuffle(selectedSessions.begin(), selectedSessions.end());
  for (auto& session : selectedSessions) {
    if (m_freeBuffers.empty()) {
      break;
    }
    while (session->nNeededBuffers() > 0 && (!m_freeBuffers.empty())) {
      session->asyncTransfer(std::move(m_freeBuffers.back()));
      --nNeededBuffers;
      m_freeBuffers.pop_back();
    }
  }

  auto now = boost::posix_time::microsec_clock::universal_time();
  m_statsCollector.updateFreeBuffers(now, m_freeBuffers.size());
  m_statsCollector.updateAssignments(now, nNeededBuffers);
}

void Input::onTimer(const boost::system::error_code& error)
{
  if (error) {
    if (error != boost::asio::error::operation_aborted) {
      ers::error(InputWatchdogError(ERS_HERE, makeSystemError(ERS_HERE, error)));
    }
    return;
  }

  auto now = std::chrono::steady_clock::now();

  {
    std::lock_guard<std::mutex> sessionsLock(m_sessionsMutex);
    for (auto& session : m_sessions) {
      session->checkTimeouts(now);
    }
  }

  m_timer->expires_at(m_timer->expires_at() + std::chrono::seconds(1));
  m_timer->async_wait(std::bind(&Input::onTimer, this, std::placeholders::_1));
}

void Input::publish()
{
  /* daq::rc::Controllable function overridden here to publish statistics
   * even if no data are received. Except for this function, stats collection
   * is indeed data-driven. So without this function, when no data are received,
   * all stats eventually would drop to zero (default value), which is
   * inaccurate for FreeBuffersAverage (at the moment it's ok for other stats as
   * they really are zero when input rate is 0).
   */

  auto const now = boost::posix_time::microsec_clock::universal_time();
  // Since m_freeBuffers is not protected, size may not be perfectly accurate
  m_statsCollector.updateFreeBuffers(now, m_freeBuffers.size());

  /* Publishing WaitingEvents would require locking m_sessionsMutex. This could
   * have an impact on performance (distributeBuffers especially). Since this
   * function is useful only when input rate drops to zero, it would eventually
   * publish zero anyway. Don't enable the following code then.
   *
   * Locking is necessary since sessions may be opened/closed anytime (DCM
   * restart for example).
   *
   * ---- DONT ----
   * std::uint32_t needed_buffers = 0;
   * {
   *   std::lock_guard<std::mutex> sessionsLock(m_sessionsMutex);
   *   for (auto& session : m_sessions) {
   *     needed_buffers += session->nNeededBuffers();
   *   }
   * }
   * m_statsCollector.updateAssignments(now, needed_buffers);
   * ---- /DONT ---- */
}

} // namespace SFOng
