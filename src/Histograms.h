#ifndef HISTOGRAMS_H
#define HISTOGRAMS_H

#include <mutex>
#include <string>
#include "monsvc/ptr.h"
class TH1I;
class TH2I;

namespace SFOng 
{

  class Histograms
  {
  public:
    Histograms();
    ~Histograms();
 
    void fillByStream(monsvc::ptr<TH1I>& histo, 
                      const std::string& stream);
    void fillByStream(monsvc::ptr<TH2I>& histo, 
                      const std::string& stream1,
                      const std::string& stream2);
    void fillByStream(monsvc::ptr<TH2I>& histo, 
                      const std::string& stream,
                      const unsigned int& value);
  
  public:
    monsvc::ptr<TH1I> eventsPerStream_raw;
    monsvc::ptr<TH1I> eventsPerStream_proc;
    monsvc::ptr<TH2I> sizePerStream_raw;
    monsvc::ptr<TH2I> sizePerStream_proc;
    monsvc::ptr<TH2I> streamCorrelation_raw;
    monsvc::ptr<TH2I> streamCorrelation_proc;
  };

}
#endif //HISTOGRAMS_H
