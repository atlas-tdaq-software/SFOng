#ifndef WEIGHTEDIOSERVICE_H
#define WEIGHTEDIOSERVICE_H

#include <cstdint>
#include <boost/asio/io_service.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <iosfwd>
#include <set>
#include <string>
#include "SlidingWindowStats.h"
#include "Tag.h"
#include "ers/Issue.h"
#include "monsvc/ptr.h"
namespace SFOng { namespace info { class SFOngWeightedIOService; } }

namespace SFOng
{

class WeightedIOService
{
public:
  WeightedIOService(
        std::size_t concurrency_hint
      , boost::posix_time::time_duration const & weight_period
      , TWatchedStreams const & watched_streams
      );
  ~WeightedIOService();

  boost::asio::io_service m_ioService;

  uint64_t weight() const;
  void addWeight(boost::posix_time::ptime const & ts, uint64_t value);

  void registerTag(Tag const &);
  void unregisterTag(Tag const &);

  void publishInfo_nolock(const std::string& obj_name, info::SFOngWeightedIOService* info);

private:
  mutable SlidingWindowStats<uint64_t> m_weightData;

  // Streams that are currently using this IOService to write to disk.
  // This allows for streams-to-thread-association tracking.
  std::set<Tag, TagCompare> m_associatedStreams;

  // Shared pointer to lock-enabled object provided by monsvc
  // Used to:
  // - lock while modifying m_associatedStreams
  // - lock while publishing (actually this happens atomically with
  //   container modification, this is why we need only one lock)
  // - remove the object from monsvc upon desctruction.
  monsvc::ptr<info::SFOngWeightedIOService> m_info;

  TWatchedStreams const & m_watchedStreams;

  void checkWatchedStreams(Tag const & t) const;
};

} // SFOng namespace

std::ostream & operator<< (std::ostream &, SFOng::WeightedIOService const &);

ERS_DECLARE_ISSUE(SFOng,
    WeightedIOServiceISPublishFailed,
    "WeightedIOService failed to publish info directly to IS",
    ERS_EMPTY)

ERS_DECLARE_ISSUE(SFOng,
    AssignPolicyFailure,
    "Watched streams are assigned to the same IO service",
    ERS_EMPTY)

#endif /* WEIGHTEDIOSERVICE_H */
