#include "Histograms.h"
#include <TAxis.h>
#include <TH1.h>
#include <TH2.h>
#include <THashList.h>
#include "monsvc/MonitoringService.h"

namespace SFOng
{

  Histograms::Histograms() {

    TH1I * histo1;
    TH2I * histo2;

    auto& monsvc = monsvc::MonitoringService::instance();

    histo1 = new TH1I("/SHIFT/SFO/EventsPerStreamRaw", "Number of events per stream", 1, -0.5, 0.5);
    histo1->SetCanExtend(TH1::kAllAxes);
    eventsPerStream_raw = monsvc.register_object(histo1->GetName(), 
                                                 histo1, true);

    histo1 = new TH1I("/SHIFT/SFO/EventsPerStreamProc", "Number of events per stream after processing",1,-0.5,0.5);
    histo1->SetCanExtend(TH1::kAllAxes);
    eventsPerStream_proc = monsvc.register_object(histo1->GetName(), 
                                                  histo1, true);
    
    histo2 = new TH2I("/SHIFT/SFO/EventSizePerStreamRaw", "Events size (B) per stream", 1, -0.5, 0.5, 500, -1500, 1498500);
    histo2->SetCanExtend(TH1::kAllAxes);
    sizePerStream_raw = monsvc.register_object(histo2->GetName(), 
                                               histo2, true);

    histo2 = new TH2I("/SHIFT/SFO/EventSizePerStreamProc", "Events size (B) per stream after processing", 1, -0.5, 0.5, 500, -1500, 1498500);
    histo2->SetCanExtend(TH1::kAllAxes);
    sizePerStream_proc = monsvc.register_object(histo2->GetName(), 
                                                histo2, true);

    histo2 = new TH2I("/SHIFT/SFO/StreamCorrelationRaw", "Stream Correlation", 1, -0.5, 0.5, 1, -0.5, 0.5);
    histo2->SetCanExtend(TH1::kAllAxes);
    streamCorrelation_raw = monsvc.register_object(histo2->GetName(), 
                                                   histo2, true);

    histo2 = new TH2I("/SHIFT/SFO/StreamCorrelationProc", "Stream Correlation after processing", 1, -0.5, 0.5, 1, -0.5, 0.5);
    histo2->SetCanExtend(TH1::kAllAxes);
    streamCorrelation_proc = monsvc.register_object(histo2->GetName(), 
                                                    histo2, true);
  }

  Histograms::~Histograms(){
    
    auto& monsvc = monsvc::MonitoringService::instance();
    
    monsvc.remove_object(eventsPerStream_raw->GetName());
    monsvc.remove_object(eventsPerStream_proc->GetName());
    monsvc.remove_object(sizePerStream_raw->GetName());
    monsvc.remove_object(sizePerStream_proc->GetName());
    monsvc.remove_object(streamCorrelation_raw->GetName());
    monsvc.remove_object(streamCorrelation_proc->GetName());
  }

  void Histograms::fillByStream(monsvc::ptr<TH1I>& histo,
                                const std::string& stream){
    histo->Fill(stream.c_str(),1);
  }

  void Histograms::fillByStream(monsvc::ptr<TH2I>& histo,
                                const std::string& stream1,
                                const std::string& stream2){
    histo->Fill(stream1.c_str(), stream2.c_str(), 1);
  }

  void Histograms::fillByStream(monsvc::ptr<TH2I>& histo,
                                const std::string& stream,
                                const unsigned int& value){
    histo->Fill(stream.c_str(), value, 1);
  }

}
