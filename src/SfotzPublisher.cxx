#include "SfotzPublisher.h"
#include <TAxis.h>
#include <TH2.h>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/system/error_code.hpp>
#include <thread>
#include <map>
#include <ostream>
#include <chrono>
#include "DFdal/SFODBConnection.h"
#include "EventStorage/DataWriterCallBack.h"
#include "EventStorage/EventStorageRecords.h"
#include "EventStorage/RawFileName.h"
#include "SFOTZ/SFOTZDataWriterCallback.h"
#include "SFOTZ/SFOTZThread.h"
#include "dal/DBConnection.h"
#include "ers/ers.h"
#include "monsvc/ptr.h"
#include "src/Config.h"
#include "src/Histograms.h"
#include "src/StatsCollector.h"
#include "src/Tag.h"
namespace SFOTZ { struct SFOTZCallback; }
namespace daq { namespace rc { class TransitionCmd; } }

namespace fs = std::filesystem;

namespace SFOng
{

namespace
{
void parseConnectionString(std::string& connectionString, 
                           unsigned int runnr,
                           const std::string& appname)
{
  
  std::string marker;
  std::string::size_type pos;
  
  // if present, replaces ${RUNNR} with the actual run number
  marker = "${RUNNR}";
  pos = connectionString.find(marker);
  if (pos != std::string::npos) {
    connectionString.replace(pos, marker.size(), std::to_string(runnr));
  }

  // if present, replaces ${APPNAME} with the application name
  marker = "${APPNAME}";
  pos = connectionString.find(marker);
  if (pos != std::string::npos) {
    connectionString.replace(pos, marker.size(), appname);
  }
}
}

SfotzPublisher::SfotzPublisher(const Config& config, const StatsCollector& stats) :
    daq::rc::Controllable(), m_callback(0), m_config(config), m_stats(stats)
{
}

void SfotzPublisher::prepareForRun(const daq::rc::TransitionCmd&)
{
  const daq::df::SFODBConnection* conf = m_config.sfotzDatabaseConnection();
  if (!conf) {
    return;
  }

  auto runnr = m_config.runParams().run_number;

  bool sfotz_queue_monitoring = conf->get_QueueMonitoringEnabled();
  ERS_LOG("SFOTZ queue monitoring enabled: " << std::boolalpha << sfotz_queue_monitoring);

  const daq::core::DBConnection* conn;
  conn = conf->get_MainDBConnection();
  if (conn) {
    ERS_DEBUG(2, "SfotzPublisher: creating main Tier0 database thread");
    std::string connString =
        SFOTZ::SFOTZThread::buildConnectionString(conn->get_Server(),
            conn->get_Name(),
            conn->get_Type());
    parseConnectionString(connString, runnr, m_config.applicationName());
    m_threads.push_back(new SFOTZ::SFOTZThread(connString,
        conn->get_User(),
        conn->get_Password(),
        conf->get_IndexTable(),
        conf->get_RunTableName(),
        conf->get_LumiBlockTableName(),
        conf->get_FileTableName(),
        conf->get_OverlapTableName(),
        false,
        sfotz_queue_monitoring));
  }

  conn = conf->get_FallbackDBConnection();
  if (conn) {
    ERS_DEBUG(2, "SfotzPublisher: creating fallback Tier0 database thread");
    std::string connString =
        SFOTZ::SFOTZThread::buildConnectionString(conn->get_Server(),
            conn->get_Name(),
            conn->get_Type());
    parseConnectionString(connString, runnr, m_config.applicationName());
    m_threads.push_back(new SFOTZ::SFOTZThread(connString,
        conn->get_User(),
        conn->get_Password(),
        conf->get_IndexTable(),
        conf->get_RunTableName(),
        conf->get_LumiBlockTableName(),
        conf->get_FileTableName(),
        conf->get_OverlapTableName(),
        true,
        sfotz_queue_monitoring));
  }
  m_callback = new SFOTZ::SFOTZDataWriterCallback(m_config.applicationName(), m_threads);

  ERS_DEBUG(2, "SfotzPublisher: starting Tier0 database threads");
  for (std::vector<SFOTZ::SFOTZThread*>::iterator iThread = m_threads.begin(); iThread != m_threads.end(); iThread++) {
    (*iThread)->start();
  }
  ERS_DEBUG(0, "SfotzPublisher::prepareForRun: threads created and started");
}

void SfotzPublisher::stopRecording(const daq::rc::TransitionCmd&)
{
  {
    std::lock_guard<std::mutex> lock(m_runTagsMutex);
    for (const auto& tag : m_runTags) {
      publishRunClosed(tag);
    }
    m_runTags.clear();
  }

  ERS_DEBUG(2, "SfotzPublisher: stopping Tier0 database threads");
  namespace pt = boost::posix_time;
  pt::ptime startTime = pt::second_clock::universal_time();
  for (std::vector<SFOTZ::SFOTZThread*>::iterator iThread = m_threads.begin(); iThread != m_threads.end(); iThread++) {
    while (!(*iThread)->empty()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      if ((pt::second_clock::universal_time() - startTime) > (m_config.actionTimeout() / 2)) {
        ers::error(IncompleteDatabase(ERS_HERE, (*iThread)->size()));
        break;
      }
    }
    (*iThread)->stop();
    (*iThread)->reset(true);
  }
  ERS_DEBUG(2, "SfotzPublisher: Tier0 database threads stopped");

  //DB threads are stopped, publish EoE statistics in the DBs
  this->publishEoEStats();

  delete m_callback;
  m_callback = 0;
  ERS_DEBUG(2, "SfotzPublisher: destroying Tier0 database threads");
  for (std::vector<SFOTZ::SFOTZThread*>::iterator iThread = m_threads.begin(); iThread != m_threads.end(); iThread++) {
    delete *iThread;
  }
  m_threads.clear();
}

void SfotzPublisher::unconfigure(const daq::rc::TransitionCmd&)
{
  //noop
}


void SfotzPublisher::onWriterCreation(const Tag& tag)
{
  auto runTag = tag.removeLumiblock();
  bool newRunTag;
  {
    std::lock_guard<std::mutex> lock(m_runTagsMutex);
    newRunTag = m_runTags.emplace(runTag).second;
  }
  if (newRunTag) {
    publishRunOpened(runTag);
  }
  publishLumiblockOpened(tag);
}

void SfotzPublisher::onWriterDestruction(const Tag& tag)
{
  publishLumiblockClosed(tag);
}

void SfotzPublisher::publishLumiblockOpened(const Tag& tag)
{
  ERS_DEBUG(3, "SfotzPublisher: publishing opened LB " << tag.fullName() << " " << tag.lumiblock);
  SFOTZ::DBInfoPointer info(new SFOTZ::DBUpdateInfo());
  info->command = SFOTZ::DBUpdateInfo::LBOPENED;
  info->runnr = m_config.runParams().run_number;
  info->sfo = m_config.applicationName();
  info->streamtype = tag.type;
  info->stream = tag.name;
  info->lbnr = tag.lumiblock;
  info->maxnrsfos = m_config.nSfos();
  publishInfo(info);
}

void SfotzPublisher::publishLumiblockClosed(const Tag& tag)
{
  ERS_DEBUG(3, "SfotzPublisher: publishing closed LB " << tag.fullName() << " " << tag.lumiblock);
  SFOTZ::DBInfoPointer info(new SFOTZ::DBUpdateInfo());
  info->command = SFOTZ::DBUpdateInfo::LBCLOSED_SPECIFIC;
  info->runnr = m_config.runParams().run_number;
  info->sfo = m_config.applicationName();
  info->streamtype = tag.type;
  info->stream = tag.name;
  info->lbnr = tag.lumiblock;
  publishInfo(info);
  ERS_DEBUG(3, "SfotzPublisher: checking LB transfer status" << tag.fullName());
  SFOTZ::DBInfoPointer transferInfo(new SFOTZ::DBUpdateInfo(*info));
  transferInfo->command = SFOTZ::DBUpdateInfo::CHECKLBTRANS;
  publishInfo(transferInfo);
}

void SfotzPublisher::publishRunOpened(const Tag& tag)
{
  ERS_DEBUG(3, "SfotzPublisher: trying to publish opened run " << tag.fullName());
  SFOTZ::DBInfoPointer info(new SFOTZ::DBUpdateInfo());
  info->command = SFOTZ::DBUpdateInfo::RUNOPENED;
  info->runnr = m_config.runParams().run_number;
  info->sfo = m_config.applicationName();
  info->streamtype = tag.type;
  info->stream = tag.name;
  info->projecttag = m_config.projectTag();
  info->maxnrsfos = m_config.nSfos();
  publishInfo(info);
}

void SfotzPublisher::publishRunClosed(const Tag& tag)
{
  ERS_DEBUG(3, "SfotzPublisher: publishing closed run " << tag.fullName());
  SFOTZ::DBInfoPointer info(new SFOTZ::DBUpdateInfo());
  info->command = SFOTZ::DBUpdateInfo::RUNCLOSED_SPECIFIC;
  info->runnr = m_config.runParams().run_number;
  info->sfo = m_config.applicationName();
  info->streamtype = tag.type;
  info->stream = tag.name;
  publishInfo(info);
  ERS_DEBUG(3, "SfotzPublisher: checking run transfer status" << tag.fullName());
  SFOTZ::DBInfoPointer transferInfo(new SFOTZ::DBUpdateInfo(*info));
  transferInfo->command = SFOTZ::DBUpdateInfo::CHECKRUNTRANS;
  publishInfo(transferInfo);
}

void SfotzPublisher::publishInfo(const SFOTZ::DBInfoPointer& info)
{
  for (std::vector<SFOTZ::SFOTZThread*>::iterator iThread = m_threads.begin(); iThread != m_threads.end(); iThread++) {
    (*iThread)->add(info);
  }
}

void SfotzPublisher::publishEoEStats()
{

  Histograms& histos = m_stats.histograms();

  TH2I * histo = histos.streamCorrelation_raw.get();

  this->parseHistoAndPublish(histo, SFOTZ::Overlap::RAW);

  histo = histos.streamCorrelation_proc.get();

  this->parseHistoAndPublish(histo, SFOTZ::Overlap::FILE);

  //Special event count entry
  std::vector<SFOTZ::OverlapData> streams;

  SFOTZ::OverlapData overlap;
  overlap.ref_stream = "EventCount";
  overlap.over_stream = "EventCount";
  overlap.overlap = m_stats.processedEvents();
  overlap.overlap_ratio = 0.0;

  streams.push_back(overlap);

  //Special physics event count entry
  overlap.ref_stream = "physics_EventCount";
  overlap.over_stream = "physics_EventCount";
  overlap.overlap = m_stats.processedPhysicsEvents();
  overlap.overlap_ratio = 0.0;

  streams.push_back(overlap);

  for (auto &thread : m_threads) {
    thread->publishEoEStats(m_config.applicationName(),
        m_config.runParams().run_number,
        streams,
        SFOTZ::Overlap::EVENTCOUNT);
  }

}

void SfotzPublisher::parseHistoAndPublish(TH2I * histo,
    SFOTZ::Overlap::Types type)
{

  std::vector<SFOTZ::OverlapData> streams;

  for (int i = 1; i <= histo->GetNbinsX(); ++i) {
    std::string xname(histo->GetXaxis()->GetBinLabel(i));
    if (xname == "")
      continue;

    //Along the diagonal we have the single stream information
    auto ref_count = histo->GetBinContent(i, i);

    for (int j = 1; j <= histo->GetNbinsY(); ++j) {
      std::string yname(histo->GetYaxis()->GetBinLabel(j));
      if (yname == "")
        continue;

      SFOTZ::OverlapData overlap;
      overlap.ref_stream = xname;
      overlap.over_stream = yname;
      overlap.overlap = (unsigned int) histo->GetBinContent(i, j);

      overlap.overlap_ratio = 0.0;
      if (overlap.overlap)
        overlap.overlap_ratio = 1.0 * overlap.overlap / ref_count;

      streams.push_back(overlap);

    }
  }

  for (auto &thread : m_threads) {
    thread->publishEoEStats(m_config.applicationName(),
        m_config.runParams().run_number,
        streams,
        type);
  }
}

void SfotzPublisher::moveStaleFiles()
{
  if(!m_threads.empty()) {
    std::vector<std::string> files = m_threads[0]->
      staleFiles(m_config.applicationName(),
                 m_config.runParams().run_number);
    
    for (const auto& filename: files) {
      
       ERS_DEBUG(1, "Cleaning file: " << filename);
       
       auto oldpath = fs::path(filename);
       auto path = oldpath.parent_path();
       auto file = oldpath.filename();
       
       if (!fs::exists(oldpath)) {
         SFOng::FileNotFound issue(ERS_HERE, oldpath);
         ers::warning(issue);
         continue;
       }

       boost::system::error_code error;
       auto size = fs::file_size(oldpath, error);
              
       daq::RawFileName rawname(file.string());
       
       auto newpath = oldpath;

       if(rawname.extension() != daq::RAWFILENAME_EXTENSION_FINISHED){
         //still .writing
         rawname.setExtension(daq::RAWFILENAME_EXTENSION_FINISHED);
         ERS_DEBUG(1, "Moving file: " << oldpath 
                   << " to " << newpath);
         newpath = path / fs::path(rawname.fileName());
         fs::rename(oldpath, newpath, error);
       }
       
       SFOTZ::DBInfoPointer info(new SFOTZ::DBUpdateInfo());
       info->command = SFOTZ::DBUpdateInfo::FILETRUNCATED;
       info->lfn = rawname.fileName();
       info->filenr = rawname.fileSequenceNumber();
       info->sfo = rawname.applicationName();
       info->runnr = rawname.runNumber();
       info->lbnr = rawname.lumiBlockNumber();
       info->streamtype = rawname.streamType();
       info->stream = rawname.streamName();
       info->filesize = size;
       info->sfopfn = newpath.string();
       
       publishInfo(info);
    }
  }
}

unsigned int SfotzPublisher::staleTags(std::unordered_map<Tag, unsigned int>& staleTags)
{
  unsigned int maxlumiblock = 0;
  if(!m_threads.empty()) {
    auto lb_stream = 
      m_threads[0]->staleLBsAndStreams(m_config.applicationName(),
                                       m_config.runParams().run_number);
    for(const auto& lbit: lb_stream) {
      
      if (lbit.first > maxlumiblock) maxlumiblock = lbit.first;
      
      for(const auto& stream: lbit.second) {
        
        Tag tag(stream.first, stream.second, lbit.first);
        
        // Store the tags in the RUN list, so that
        // we will close these entries at the end of run
        // in case no more event for these tags is received
        auto runTag = tag.removeLumiblock();
        {
          std::lock_guard<std::mutex> lock(m_runTagsMutex);
          m_runTags.emplace(runTag);
        }
        
        unsigned int index = 
          m_threads[0]->getMaxIndex(m_config.applicationName(),
                                    m_config.runParams().run_number,
                                    lbit.first,
                                    stream.first,
                                    stream.second);
        // Provide the _next_ available index for each tag
        staleTags[tag] = index+1;
      }
    }
  }
  return maxlumiblock;
}

bool SfotzPublisher::registerSFOTZCallbackToMainDbThread(SFOTZ::SFOTZCallback* cb_obj, std::set<SFOTZ::DBUpdateInfo::CommandType> const & command_types)
{
  if (m_threads.empty()) {
    ERS_LOG("SfotzPublisher: no threads to register callback object to");
    return false;
  }
  if (!m_threads.front()->connected()) {
    ERS_LOG("SfotzPublisher: main thread is not connected: skip registering callback");
    return false;
  }
  m_threads.front()->registerCallback(cb_obj, command_types);
  return true;
}

} // namespace SFOng
