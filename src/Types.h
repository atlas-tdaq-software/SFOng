#ifndef TYPES_H
#define TYPES_H

#include <sys/uio.h> // For iovec

#include <cstdint>
#include <memory>
#include <vector>

namespace SFOng
{

typedef std::vector<std::uint8_t> Buffer;
typedef std::shared_ptr<Buffer> BufferHandle;

class Event;
typedef std::shared_ptr<Event> EventHandle;

typedef std::vector<iovec> IoVector;
inline IoVector IoVector1(const iovec& iovec)
{
  return IoVector(1, iovec);
}
inline IoVector IoVector1(void* data, std::size_t size)
{
  return IoVector1({ data, size });
}
inline IoVector IoVector1(Buffer& b)
{
  return IoVector1(b.data(), b.size());
}

class WriterSerializer;
typedef std::shared_ptr<WriterSerializer> WriterSerializerHandle;

} // namespace SFOng

#endif // TYPES_H
