#ifndef SFONG_CLEANER_H
#define SFONG_CLEANER_H

#include "RunControl/Common/Controllable.h"


namespace SFOng
{

struct Config;
struct StatsCollector;
struct SfotzPublisher;
struct WritingManager;
struct Input;

/**
 * @brief Proper termination of the SFOng application
 * 
 * Class that implements daq::rc::Controllable::onExit(..) called when
 * the application is requested to terminated (SIGTERM, SIGINT). It has
 * references to all (controllable) high-level objects. 
 * 
 * An object is to be created and added last for the rc::Dispatcher.
 *
 * The onExit method of the controllable interface is currently invoked in the
 * registration order even if an "InverseDispatcher" is used. This is not how
 * we can handle termination. This made this class necessary.
 */
struct Cleaner : public daq::rc::Controllable
{
  Cleaner(Config&, StatsCollector&, SfotzPublisher&, WritingManager&, Input&);
  
  void onExit(daq::rc::FSM_STATE) noexcept override;

private:
  Config& m_config;
  StatsCollector& m_statsCollector;
  SfotzPublisher& m_sfotzPublisher;
  WritingManager& m_writingManager;
  Input& m_input;
};

} // SFOng namesapce

#endif // SFONG_CLEANER_H