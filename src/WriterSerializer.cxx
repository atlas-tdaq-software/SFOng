#include "WriterSerializer.h"
#include <boost/smart_ptr/shared_ptr.hpp>
#include <string>
#include "Config.h"
#include "EventStorage/DataWriter.h"
#include "EventStorage/ESCompression.h"
#include "EventStorage/EventStorageRecords.h"
#include "EventStorage/RawFileName.h"
#include "SfotzPublisher.h"
#include "src/Tag.h"
#include "src/WeightedIOService.h"
namespace EventStorage { class DataWriterCallBack; }

namespace SFOng
{

WriterSerializer::WriterSerializer(const Config& config,
                                   WeightedIOService& io_service,
                                   SfotzPublisher& sfotzPublisher,
                                   const Tag& tag, 
                                   const std::filesystem::path& path,
                                   const int index,
                                   SlidingWindowStats<uint64_t> & stream_weight,
                                   EventStorage::DataWriterCallBack* dir_manager) :
  m_strand(io_service.m_ioService),
  m_ioService(io_service),
  m_sfotzPublisher(sfotzPublisher),
  m_tag(tag),
  m_streamWeight(stream_weight)
{
  auto rawfilename = boost::shared_ptr<daq::RawFileName>
    (new daq::RawFileName(config.projectTag(), 
                          config.runParams().run_number, 
                          tag.type, tag.name, 
                          tag.lumiblock, config.applicationName()));

  if (index) {
    rawfilename->setFileSequenceNumber(index);
  }

  m_writer.reset(new EventStorage::DataWriter(path.string(),
                                              rawfilename,
                                              config.runParams(),
                                              config.projectTag(),
                                              tag.type, tag.name, 
                                              tag.fullName(), tag.lumiblock, 
                                              config.applicationName(),
                                              config.metaDataStrings(),
                                              EventStorage::NONE, 
                                              config.compressionLevel(),
                                              config.adler32MtNbThreads(),
                                              config.adler32MtThreshold()));

  if (config.maxFileSize_MiB()) {
    m_writer->setMaxFileMB(config.maxFileSize_MiB());
  }
  if (config.maxEventsPerFile()) {
    m_writer->setMaxFileNE(config.maxEventsPerFile());
  }
  m_sfotzPublisher.onWriterCreation(m_tag);
  m_writer->registerCallBack(m_sfotzPublisher.dataWriterCallback());
  m_writer->registerCallBack(dir_manager);

  // Register association made between stream (i.e. tag, here it includes LB)
  // and thread
  m_ioService.registerTag(m_tag);

  // TODO: adjust file sequence number if recovering from a crash (see old SFO DataStream.cxx m_lb_init_seq)
}

WriterSerializer::~WriterSerializer()
{
  // Explicitly delete the writer, so that 
  // a) we set the last SFOTZ file entry to CLOSED via the
  // dataWriterCallback *before* before we set the SFOTZ 
  // lumiblock entry to CLOSED in SfotzPublisher::onWriterDestruction
  // b) the last file has the end of sequence flag set
  m_writer.reset();
  m_sfotzPublisher.onWriterDestruction(m_tag);

  // Unregister stream to thread association
  m_ioService.unregisterTag(m_tag);
}

} // namespace SFOng
