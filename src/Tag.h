#ifndef TAG_H
#define TAG_H

#include <algorithm> // for std::swap
#include <functional> // for std::hash
#include <string>

#include "eformat/StreamTag.h"

namespace SFOng
{

struct Tag
{

  Tag() :
      lumiblock(0)
  {
  }

  Tag(const std::string& t, const std::string& n, uint16_t lb = 0) :
      type(t), name(n), lumiblock(lb)
  {
  }

  explicit Tag(const eformat::helper::StreamTag& streamTag, uint16_t lb = 0) :
      type(streamTag.type), name(streamTag.name), lumiblock(streamTag.obeys_lumiblock ? lb : 0)
  {
  }

  std::string fullName() const
  {
    return type + "_" + name;
  }

  Tag removeLumiblock() const
  {
    return Tag(type, name);
  }

  bool operator==(const Tag& rhs) const
  {
    return type == rhs.type && name == rhs.name && lumiblock == rhs.lumiblock;
  }

  bool sameStream(const Tag& rhs) const {
    return type == rhs.type && name == rhs.name;
  }

  std::string type;
  std::string name;
  uint16_t lumiblock;

  explicit operator std::size_t() const
  {
	  return std::hash<std::string>{}(type) ^ std::hash<std::string>{}(name) ^ std::hash<uint16_t>{}(lumiblock);
  }
};

inline void swap(Tag& a, Tag& b)
{
  using std::swap;

  swap(a.type, b.type);
  swap(a.name, b.name);
  swap(a.lumiblock, b.lumiblock);
}

// Hash for TBB
inline std::size_t tag_hasher(const Tag& a)
{
  return static_cast<std::size_t>(a);
}

// TBB hasher that does not take LB into account
struct TbbTagHasherNolb
{
  bool equal(Tag const & a, Tag const & b) const {
    if (hash(a) == hash(b)) {
      return true;
    }
    return false;
  }

  std::size_t hash(Tag const & a) const {
    return std::hash<std::string>{}(a.type) ^ std::hash<std::string>{}(a.name);
  }
};

// Hash for Boost
inline std::size_t hash_value(const Tag& a)
{
  return static_cast<std::size_t>(a);
}

struct TagCompare
{
  bool operator()(Tag const & lhs, Tag const & rhs) const
  {
    if (lhs.type.compare(rhs.type) < 0) return true;
    if (lhs.type.compare(rhs.type) > 0) return false;
    // lhs.type == rhs.type
    if (lhs.name.compare(rhs.name) < 0) return true;
    if (lhs.name.compare(rhs.name) > 0) return false;
    // AND lhs.name == rhs.name
    return lhs.lumiblock < rhs.lumiblock;
  }
};

struct TagCompareNoLb
{
  bool operator()(Tag const & lhs, Tag const & rhs) const
  {
    if (lhs.type.compare(rhs.type) < 0) return true;
    if (lhs.type.compare(rhs.type) > 0) return false;
    return lhs.name.compare(rhs.name) < 0;
  }
};

typedef std::map<Tag, std::set<Tag, TagCompareNoLb>, TagCompareNoLb> TWatchedStreams;

} // namespace SFOng

// Hash for STL
namespace std
{

template<>
struct hash<SFOng::Tag>
{
  inline std::size_t operator()(const SFOng::Tag& a) const
  {
    return static_cast<std::size_t>(a);
  }
};

} // namespace std

inline std::ostream & operator<<(std::ostream & os, SFOng::Tag const & t)
{
  os << '[' << t.type << ", " << t.name << ", " << t.lumiblock << ']';
  return os;
}

#endif // TAG_H
