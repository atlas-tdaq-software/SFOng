#include "Event.h"
#include <list>
#include <set>
#include <string>
#include <unordered_set>
#include "Input.h"
#include "StatsCollector.h"
#include "Types.h"
#include "eformat/EnumClass.h"
#include "eformat/FullEventFragmentNoTemplates.h"
#include "eformat/Issue.h"
#include "eformat/ROBFragmentNoTemplates.h"
#include "eformat/compression.h"
#include "eformat/write/FullEventFragment.h"
#include "eformat/write/node.h"
#include "src/Tag.h"

#ifndef ERS_NO_DEBUG
#include <sstream>
#include "utils/buffer.h"
#endif // !defined(ERS_NO_DEBUG)

namespace ef = eformat;
namespace efh = eformat::helper;
namespace efr = eformat::read;
namespace efw = eformat::write;
namespace pt = boost::posix_time;

namespace SFOng
{

Event::Event(StatsCollector& sc, Input& i, std::uint64_t gid, BufferHandle b) :
    m_input(i),
    m_statsCollector(sc),
    m_creationTimestamp(pt::microsec_clock::universal_time()),
    m_buffer(b),
    m_gid(gid),
    m_lb(0),
    m_rawSize(0),
    m_fullEventUncompressedSize(0)
{
}

Event::Event(StatsCollector& sc, Input& in, 
             std::uint64_t gid, std::uint32_t run_number, 
             BufferHandle b, bool lumiblockEnabled) :
    m_input(in),
    m_statsCollector(sc),
    m_creationTimestamp(pt::microsec_clock::universal_time()),
    m_buffer(b),
    m_gid(gid),
    m_lb(0),
    m_rawSize(0),
    m_fullEventUncompressedSize(0)
{
  const std::uint32_t* const begin = reinterpret_cast<uint32_t *>(m_buffer->data());
  const std::uint32_t* const end = reinterpret_cast<uint32_t *>(m_buffer->data() + m_buffer->size());
  const std::uint32_t* buf = begin;

  std::vector<efh::StreamTag> streamTags;
  std::set<std::uint32_t> streamTagsIndexes;
  while (buf < end) {

    // Parse the event fragment
    efr::FullEventFragment fe(buf);
    try {
      fe.check();
    }
    catch (eformat::Issue& e) {
      throw MalformedEvent(ERS_HERE, m_gid, "Event format error", e);
    }

    // Check if the GID is consistent with the one provided
    if (fe.global_id() != m_gid) {
      throw InconsistentGIDs(ERS_HERE, m_gid, fe.global_id());
    }
    
    // Check the run number
    if (fe.run_no() != run_number) {
      throw InconsistentRunNumber(ERS_HERE, fe.run_no(), run_number);
    }

    m_rawSize += fe.fragment_size_word() * sizeof(std::uint32_t);

    if (buf == begin) {
      // Take the LB and stream tags of the first fragment as authoritative
      m_lb = fe.lumi_block();
      efh::decode(fe.nstream_tag(), fe.stream_tag(), streamTags);
      // Check for invalid stream tags
      std::unordered_set<Tag> tags;
      for (const auto& streamTag : streamTags) {
        if (streamTag.type.empty()) {
          throw MalformedEvent(ERS_HERE, m_gid, "Empty stream tag type");
        }
        if (streamTag.name.empty()) {
          throw MalformedEvent(ERS_HERE, m_gid, "Empty stream tag name");
        }
        if (tags.insert(Tag(streamTag, 0)).second == false) {
          throw MalformedEvent(ERS_HERE, m_gid, "Duplicate stream tags");
        }
      }
    }
    else {
      // Check if LB and stream tags of the following fragments are consistent with the first
      if (fe.lumi_block() != m_lb) {
        throw InconsistentLBs(ERS_HERE, m_gid, m_lb, fe.lumi_block());
      }
      std::vector<efh::StreamTag> streamTags2;
      efh::decode(fe.nstream_tag(), fe.stream_tag(), streamTags2);
      if (streamTags2 != streamTags) {
        throw InconsistentStreamTags(ERS_HERE, m_gid);
      }
    }

    // Go to the "active" stream tags indexes list
    buf = fe.end();
    if (buf >= end) {
      throw MalformedEvent(ERS_HERE, m_gid, "Premature end of message");
    }

    // Parse the "active" stream tags indexes
    const std::uint32_t streamTagsIndexesSize = *buf;
    ++buf;
    const std::uint32_t* streamTagsIndexesEnd = buf + streamTagsIndexesSize;
    if (streamTagsIndexesEnd > end) {
      throw MalformedEvent(ERS_HERE, m_gid, "Premature end of message");
    }

    m_views.emplace_back();
    m_views.back().iov = iovec { const_cast<std::uint32_t*>(fe.start()), fe.fragment_size_word() * sizeof(std::uint32_t) };

    m_views.back().uncompressedDataSize = (fe.header_size_word() + fe.readable_payload_size_word()) * sizeof(uint32_t);

    for (; buf < streamTagsIndexesEnd; ++buf) {
      std::uint32_t i = *buf;
      if (i >= streamTags.size()) {
        throw MalformedEvent(ERS_HERE, m_gid, "Invalid active stream tags indexes");
      }
      if (streamTagsIndexes.insert(i).second == false) {
        throw MalformedEvent(ERS_HERE, m_gid, "Duplicate active stream tags indexes");
      }
      m_views.back().streamTags.push_back(streamTags[i]);
    }
  }
  if (m_views.empty()) {
    throw MalformedEvent(ERS_HERE, m_gid, "No active stream tags found");
  }

  for (const auto& view : m_views) {
    for (const auto& streamTag : view.streamTags) {
      m_rawStreams.emplace_back();
      m_rawStreams.back().tag = Tag(streamTag, lumiblockEnabled ? m_lb : 0);
      m_rawStreams.back().iovector = IoVector1(view.iov);
      m_rawStreams.back().uncompressedDataSize = view.uncompressedDataSize;
    }

    m_fullEventUncompressedSize += view.uncompressedDataSize;
  }

  m_fileStreams = m_rawStreams;

  m_statsCollector.onEventCreation(m_creationTimestamp, *this);
}

Event::Event(StatsCollector& sc, Input& i, std::uint64_t gid, BufferHandle b, const Tag& singleTag) :
    Event(sc, i, gid, b)
{
  m_rawSize = m_buffer->size();
  // There's nothing better we can do at this point
  m_fullEventUncompressedSize = m_buffer->size();

  m_rawStreams.resize(1);
  m_rawStreams.back().tag = singleTag;
  m_rawStreams.back().iovector = IoVector1(*m_buffer);

  m_fileStreams = m_rawStreams;

  m_statsCollector.onEventCreation(m_creationTimestamp, *this);
}

void Event::toSingleFileStream(const Tag& singleTag)
{
  m_fileStreams.resize(1);
  m_fileStreams.back().tag = singleTag;
  if (m_views.empty()) {
    // if m_views is empty the event is corrupt and we shouldn't try to access its data
    // we use the full buffer as stream
    m_fileStreams.back().iovector = IoVector1(*m_buffer);
  }
  else {
    efw::FullEventFragment singleFe;
    std::set<std::uint32_t> robIds;
    singleFe.copy_header(reinterpret_cast<std::uint32_t*>(m_views[0].iov.iov_base));

    // We need to keep these alive until the ROBs are copied
    // because of the de-compression buffers.
    // We use a list to avoid implicit re-allocations (like
    // for e.g a vector). These call the FullEventFragment 
    // copy constructor, resulting in the stored ROB pointers
    // to be come invalid.
    std::list<efr::FullEventFragment> fullEvents;
    for (const auto& view : m_views) {

      fullEvents.emplace_back(reinterpret_cast<std::uint32_t*>(view.iov.iov_base));
      std::vector<efr::ROBFragment> robs;
      fullEvents.back().robs(robs);
      for (const auto& rob : robs) {
        if (robIds.insert(rob.source_id()).second == true) {
          singleFe.append_unchecked(rob.start());
        }
      }
    }
    // Do not re-compress this event
    singleFe.compression_type(ef::UNCOMPRESSED);
    auto node = singleFe.bind();
    m_singleFragment.reset(new std::uint32_t[singleFe.size_word()]);
    efw::copy(*node, m_singleFragment.get(), singleFe.size_word());
    m_fileStreams.back().iovector = IoVector1(
        m_singleFragment.get(), singleFe.size_word() * sizeof(std::uint32_t));
  }
}

Event::~Event()
{
  m_input.returnBuffer(m_buffer);
  auto now = pt::microsec_clock::universal_time();
  pt::time_duration lifeTime = now - m_creationTimestamp;
  m_statsCollector.onEventDestruction(now, *this, lifeTime);
}

#ifndef ERS_NO_DEBUG
std::string Event::debugStr(bool withStreams) const
    {
  std::ostringstream ostr;
  ostr << "[gid: " << gid();
  if (withStreams) {
    for (const auto& stream : m_rawStreams) {
      ostr << ", raw stream: " << stream.debugStr();
    }
    for (const auto& stream : m_fileStreams) {
      ostr << ", file stream: " << stream.debugStr();
    }
  }
  ostr << "]";
  return ostr.str();
}

std::string EventStream::debugStr() const
{
  std::ostringstream ostr;
  ostr << "[name: " << tag.fullName() << ", size: " << utils::buffers_size(iovector) << " kB]";
  return ostr.str();
}
#endif // !defined(ERS_NO_DEBUG)

}
// namespace SFOng
