#include <exception>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include "Config.h"
#include "Input.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/ItemCtrl/ControllableDispatcher.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "SFOng/dal/SFOngApplication.h"
#include "SfotzPublisher.h"
#include "StatsCollector.h"
#include "TH1.h"
#include "TThread.h"
#include "TSystem.h"
#include "WritingManager.h"
#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/exceptions.h"
#include "Cleaner.h"

using namespace SFOng;

int main(int argc, char* argv[])
{
  try {
    IPCCore::init(argc, argv);
  }
  catch (const daq::ipc::AlreadyInitialized&) {
    // ignore
  }

  //Remove ROOT SigHandlers
  for (int sig = 0; sig < kMAXSIGNALS; sig++)
  {
    gSystem->ResetSignal((ESignals)sig);
  }

  //Prevent histogram tracking
  TH1::AddDirectory(kFALSE);

  //Initialise mutexes
  TThread::Initialize();


  std::unique_ptr<daq::rc::CmdLineParser> cmdParser;

  try {
    cmdParser.reset(new daq::rc::CmdLineParser(argc, argv, true));
  }
  catch (const daq::rc::CmdLineHelp& ex) {
    std::cout << ex.message() << std::endl;
    return EXIT_SUCCESS;
  }
  catch (const daq::rc::CmdLineError& ex) {
    ers::fatal(ex);
    return EXIT_FAILURE;
  }
  catch (const std::exception& ex) {
    ers::fatal(daq::rc::CmdLineError(ERS_HERE, "", ex));
    return EXIT_FAILURE;
  }

  auto config = std::make_shared<Config>();
  auto statsCollector = std::make_shared<StatsCollector>(*config);
  auto sfotzPublisher = std::make_shared<SfotzPublisher>(*config, *statsCollector);
  auto writingManager = std::make_shared<WritingManager>(*config, *sfotzPublisher, *statsCollector);
  auto input = std::make_shared<Input>(*config, *writingManager, *statsCollector);
  auto cleaner = std::make_shared<Cleaner>(*config, *statsCollector, *sfotzPublisher, *writingManager, *input);

  daq::rc::ControllableDispatcher::ControllableList ctrls;
  ctrls.push_back(config);
  ctrls.push_back(sfotzPublisher);
  ctrls.push_back(statsCollector);
  ctrls.push_back(writingManager);
  ctrls.push_back(input);
  ctrls.push_back(cleaner);

  auto dispatcher = std::make_shared<daq::rc::InverseDispatcher>();

  daq::rc::ItemCtrl ctrl(*cmdParser, ctrls, dispatcher);
  ctrl.init();
  ctrl.run();

  // cleanup is done by cleaner Controllable which is the only one implementing
  // daq::rc::Controllable::onExit

  return EXIT_SUCCESS;
}
