#ifndef UTILS_IOVECS_H
#define UTILS_IOVECS_H

#include <numeric>

#include <sys/uio.h>

namespace utils
{

template<typename PointerToPodType>
inline PointerToPodType buffer_cast(const iovec& iov)
{
  return static_cast<PointerToPodType>(iov.iov_base);
}

template<typename PointerToPodType>
inline PointerToPodType buffer_cast(iovec& iov)
{
  return static_cast<PointerToPodType>(iov.iov_base);
}

inline std::size_t buffer_size(const iovec& iov)
{
  return iov.iov_len;
}

template<typename BufferSequence>
inline std::size_t buffers_size(const BufferSequence& buffers)
{
  return std::accumulate(buffers.begin(), buffers.end(), 0,
      [] (const std::size_t& a, typename BufferSequence::const_reference b) {
        return a + buffer_size(b);
      });
}

} // namespace utils

#endif // UTILS_IOVECS_H
