#if defined(BOOST_ENABLE_ASSERT_HANDLER) || TBB_USE_ASSERT

#include <cstdlib>
#include "ers/ers.h"
#endif // defined(BOOST_ENABLE_ASSERT_HANDLER) || TBB_USE_ASSERT

#ifdef BOOST_ENABLE_ASSERT_HANDLER
namespace boost
{
// for this to be actually invoked by Boost the BOOST_ENABLE_ASSERT_HANDLER macro must be defined
void assertion_failed(const char* expression, const char* function, const char* file, long line)
{
  ers::Assertion issue(ers::LocalContext("Boost", file, line, function), expression,
      "of grave misuse of the Boost libraries");
  ers::fatal(issue);
  std::abort();
}
} // namespace boost
#endif // defined(BOOST_ENABLE_ASSERT_HANDLER)

// tbb::set_assertion_handler was removed as from TBB 2021.1
// there is apparently no other way to set an assertion handler
#if 0
#if TBB_USE_ASSERT
#include <tbb/tbb_stddef.h>
namespace
{
struct TbbAssertionHandler
{
  TbbAssertionHandler()
  {
    tbb::set_assertion_handler(handler);
  }
  static void handler(const char* file, int line, const char* expression, const char* comment)
  {
    ers::Assertion issue(ers::LocalContext("TBB", file, line, "(unknown)"), expression, comment);
    ers::fatal(issue);
    std::abort();
  }
} tbbAssertionHandler;
} // namespace
#endif // TBB_USE_ASSERT
#endif
