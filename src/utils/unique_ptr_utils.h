#ifndef UTILS_UNIQUE_PTR_UTILS_H
#define UTILS_UNIQUE_PTR_UTILS_H

#include <memory>
#include <type_traits>

namespace utils
{

//-------------
// make_unique
//-------------

namespace detail
{

template<class T>
struct _Unique_if
{
  typedef std::unique_ptr<T> _Single_object;
};

template<class T>
struct _Unique_if<T[]>
{
  typedef std::unique_ptr<T[]> _Unknown_bound;
};

template<class T, size_t N>
struct _Unique_if<T[N]>
{
  typedef void _Known_bound;
};

} // namespace detail

template<class T, class ... Args>
typename detail::_Unique_if<T>::_Single_object make_unique(Args&&... args)
{
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

template<class T>
typename detail::_Unique_if<T>::_Unknown_bound make_unique(size_t n)
{
  typedef typename std::remove_extent<T>::type U;
  return std::unique_ptr<T>(new U[n]());
}

template<class T, class ... Args>
typename detail::_Unique_if<T>::_Known_bound make_unique(Args&&...) = delete;

//---------------------
// static_pointer_cast
//---------------------

template<class T1, class T2>
std::unique_ptr<T1> static_pointer_cast(std::unique_ptr<T2>&& ptr)
{
  return std::unique_ptr<T1>(static_cast<T1*>(ptr.release()));
}

} // namespace utils

#endif // !defined(UTILS_UNIQUE_PTR_UTILS_H)
