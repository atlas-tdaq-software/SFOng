#include "Config.h"
#include <boost/algorithm/string/classification.hpp> // for is_any
#include <boost/algorithm/string/split.hpp>
#include <thread>
#include <limits>
#include <map>
#include <set>
#include <sstream>
#include "dal/BaseApplication.h"
#include "dal/Computer.h"
#include "eformat/DetectorMask.h"
#include "ers/ers.h"
#include "is/exceptions.h"
#include "is/infoT.h"
#include "is/infodictionary.h"
#include "rc/RunParams.h"
#include "src/Tag.h"
namespace daq { namespace rc { class TransitionCmd; } }

namespace SFOng
{

Config::Config() :
    daq::rc::Controllable()
{
  m_computedParams.fakeRunNo = 0;
  m_computedParams.nSfos = 0;
  m_computedParams.writingEnabled = false;
}

void Config::configure(const daq::rc::TransitionCmd&)
{
  parseSecretParams();

  m_computedParams.fakeRunNo++;

  // calculate the number of enabled SFOs in the partition
  m_computedParams.nSfos = 0;
  std::set<std::string> app_classes;
  app_classes.insert("SFOngApplication");
  std::vector<const daq::core::BaseApplication*> apps = partition()->get_all_applications(&app_classes);
  for (const auto& app : apps) {
    const dal::SFOngApplication* sfo_app = database().cast<dal::SFOngApplication>(app);
    if (!sfo_app->disabled(*partition()) && app->get_host()->get_State()) {
      m_computedParams.nSfos++;
    }
  }

  m_computedParams.writingEnabled = configuration()->get_RecordingEnabled();

  // convert various parameters to a more usable form
  m_derivedParams.actionTimeout = boost::posix_time::seconds(application()->get_ActionTimeout());
  m_derivedParams.corruptEventsTag = Tag(m_secretParams.corruptEventsTagType, m_secretParams.corruptEventsTagName);
  m_derivedParams.eventTransferTimeout = std::chrono::milliseconds(configuration()->get_EventTransferTimeout_ms());
  m_derivedParams.filesystemTrackerGranularity = boost::posix_time::seconds(
      m_secretParams.filesystemTrackerGranularity_s);
  m_derivedParams.ipcPartition = IPCPartition(partition()->UID());
  m_derivedParams.lateEventsTag = Tag(m_secretParams.lateEventsTagType, m_secretParams.lateEventsTagName);
  if (configuration()->get_NInputThreads() == 0) {
    m_derivedParams.nInputThreads = std::thread::hardware_concurrency();
  }
  else {
    m_derivedParams.nInputThreads = configuration()->get_NInputThreads();
  }
  if (configuration()->get_NWorkerThreads() == 0) {
    m_derivedParams.nWorkerThreads = std::thread::hardware_concurrency();
  }
  else {
    m_derivedParams.nWorkerThreads = configuration()->get_NWorkerThreads();
  }
  m_derivedParams.statsSamplerGranularity = boost::posix_time::milliseconds(
      m_secretParams.statsSamplerGranularity_ms);
  m_derivedParams.writingPathChangeTime = boost::posix_time::minutes(configuration()->get_DirectoryChangeTime_min());
  m_derivedParams.writingPathLifetime = boost::posix_time::minutes(configuration()->get_DirectoryWritingTime_min());

  // AssignPolicy
  m_derivedParams.ioServiceAssignPolicy = AssignPolicy::OccupancyWeighted;
  std::string const & assign_policy_str = configuration()->get_AssignPolicy();
  if (assign_policy_str == "RoundRobin")
    m_derivedParams.ioServiceAssignPolicy = AssignPolicy::RoundRobin;
  else if (assign_policy_str == "Occupancy")
    m_derivedParams.ioServiceAssignPolicy = AssignPolicy::OccupancyWeighted;
  else if (assign_policy_str == "DebugAssignErrors")
    m_derivedParams.ioServiceAssignPolicy = AssignPolicy::DebugAssignErrors;

  // else: the schema says it's an enum so it cannot be something else

  m_derivedParams.streamWeightPeriod = configuration()->get_StreamWeightPeriod();
  m_derivedParams.ioServiceWeightPeriod = configuration()->get_IOServiceWeightPeriod();

  ERS_LOG("AssignPolicy: " << m_derivedParams.ioServiceAssignPolicy
      << ", StreamWeightPeriod: " << m_derivedParams.streamWeightPeriod
      << ", IOServiceWeightPeriod: " << m_derivedParams.ioServiceWeightPeriod
      );

  for (TWatchedStreams::const_iterator it(m_secretParams.assignPolicyWatchedStreams.begin());
      it != m_secretParams.assignPolicyWatchedStreams.end(); ++it)
  {
    std::ostringstream oss;
    oss << it->first << " is incompatible with: ";
    for (Tag const & t: it->second) {
      oss << t << ", ";
    }
    ERS_LOG("WatchedStreams: " << oss.str());
  }

  ERS_LOG("FileSystemTrackerGranularity: " << m_secretParams.filesystemTrackerGranularity_s << " s");

  /*
    This variable allows randomising the disk rotation across
    different SFOs. However, if we want to run multiple applications
    on the same node, we cannot use a random function (in fact we
    want to keep the applications sharing a node synchronized
    as much as possible)
    So we calculate a deterministic function hashing the hostname
    The hash is used to calculate an host-specific offset between
    nor and writingPathLifetime/2
  */

  m_derivedParams.writingPathLifetimeOffset = 
    boost::posix_time::minutes(0);
  
  if (m_secretParams.randomizeRotation) {
    std::size_t const host_hash = std::hash<std::string>()(daq::rc::OnlineServices::instance().getHostName());
    float const fraction = static_cast<float>(host_hash)/static_cast<float>(std::numeric_limits<std::size_t>::max());
    m_derivedParams.writingPathLifetimeOffset = 
        boost::posix_time::minutes(static_cast<long>(fraction*(configuration()->get_DirectoryWritingTime_min()/2)));
    ERS_LOG("Rotation offset (minutes): " << m_derivedParams.writingPathLifetimeOffset.minutes());
  }
}

void Config::prepareForRun(const daq::rc::TransitionCmd&)
{
  ISInfoDictionary infoDict(m_derivedParams.ipcPartition);

  ERS_DEBUG(1, "SFO updates parameters obtained from IS.");
  m_derivedParams.metaDataStrings.clear();

  RunParams rpi;
  try {
    infoDict.findValue("RunParams.RunParams", rpi);
    m_derivedParams.runParams.run_number = rpi.run_number;
    m_derivedParams.runParams.max_events = rpi.max_events;
    m_derivedParams.runParams.rec_enable = rpi.recording_enabled;
    m_derivedParams.runParams.trigger_type = rpi.trigger_type;
    eformat::helper::DetectorMask mask(rpi.det_mask);
    m_derivedParams.runParams.detector_mask_MS = mask.serialize().first;
    m_derivedParams.runParams.detector_mask_LS = mask.serialize().second;
    m_derivedParams.runParams.beam_type = rpi.beam_type;
    m_derivedParams.runParams.beam_energy = rpi.beam_energy;
    m_derivedParams.projectTag = rpi.T0_project_tag;
    if (rpi.recording_enabled != m_computedParams.writingEnabled) {
      std::string iguiDesc = rpi.recording_enabled ? "enabled" : "disabled";
      std::string confDesc = m_computedParams.writingEnabled ? "enabled" : "disabled";
      std::string err = "Data recording is " + iguiDesc + " in the IGUI and " + confDesc
          + " in the configuration data base. It will be disabled.";
      ers::warning(ConfigIssue(ERS_HERE, err));
      m_computedParams.writingEnabled = false;
    }
  } catch (daq::is::Exception & ex) {
    ers::error(ConfigIssue(ERS_HERE, "Could not access IS information RunParams.RunParams. Using bogus parameters."));
    m_derivedParams.runParams.run_number = m_computedParams.fakeRunNo;
    m_derivedParams.runParams.max_events = 0;
    m_derivedParams.runParams.rec_enable = 0;
    m_derivedParams.runParams.trigger_type = 0;
    m_derivedParams.runParams.detector_mask_LS = 0;
    m_derivedParams.runParams.detector_mask_MS = 0;
    m_derivedParams.runParams.beam_type = 0;
    m_derivedParams.runParams.beam_energy = 0;
    m_derivedParams.projectTag = "data_test";
  }

  try {
    ISInfoT<std::vector<std::string> > mds;
    infoDict.findValue("RunParams.UserMetaData", mds);
    m_derivedParams.metaDataStrings = mds.getValue();
  } catch (daq::is::Exception & ex) {
    ERS_LOG("RunParams.UserMetaData strings not found in IS.");
    m_derivedParams.metaDataStrings.clear();
  }
}

void Config::stopRecording(const daq::rc::TransitionCmd&)
{
  m_computedParams.writingEnabled = configuration()->get_RecordingEnabled();
  StopMutex::getMutex().unlock();
}

void Config::unconfigure(const daq::rc::TransitionCmd&)
{
  StopMutex::getMutex().unlock();
}

void Config::parseSecretParams()
{
  // Defaults
  m_secretParams.corruptEventsTagName = "SFOCorruptedEvents";
  m_secretParams.corruptEventsTagType = "debug";
  m_secretParams.filesystemTrackerGranularity_s = 60;
  m_secretParams.randomizeRotation = false;
  m_secretParams.lateEventsTagName = "LateEvents";
  m_secretParams.lateEventsTagType = "debug";
  m_secretParams.statsSamplerGranularity_ms = 500;

  for (const auto& parameter : configuration()->get_ExtraParameters())
  {
    std::vector<std::string> kv;
    boost::split(kv, parameter, boost::is_any_of("="));
    if (kv.size() != 2 || kv[1] == "")
    {
      ERS_LOG("Invalid extra parameter " << parameter << ". Ignored.");
      continue;
    }
    std::istringstream iss(kv[1]);
    if (kv[0] == "CorruptEventsTagName") {
      iss >> m_secretParams.corruptEventsTagName;
    }
    else if (kv[0] == "CorruptEventsTagType") {
      iss >> m_secretParams.corruptEventsTagType;
    }
    else if (kv[0] == "FilesystemTrackerGranularity_s") {
      iss >> m_secretParams.filesystemTrackerGranularity_s;
    }
    else if (kv[0] == "RandomizeRotation") {
      iss >> m_secretParams.randomizeRotation;
    }
    else if (kv[0] == "LateEventsTagName") {
      iss >> m_secretParams.lateEventsTagName;
    }
    else if (kv[0] == "LateEventsTagType") {
      iss >> m_secretParams.lateEventsTagType;
    }
    else if (kv[0] == "StatsSamplerGranularity_ms") {
      iss >> m_secretParams.statsSamplerGranularity_ms;
    }
    else if (kv[0] == "AssignPolicyWatchedStreamsPair") {
      // format: AssignPolicyWatchedStreamPair=physics_Main;physics_delayed

      std::vector<std::string> streams;
      boost::split(streams, kv[1], boost::is_any_of(";"));
      if (streams.size() != 2) {
        ERS_LOG("must have at 2 streams");
        continue;
      }

      std::vector<std::string> p;
      boost::split(p, streams[0], boost::is_any_of("_"));
      if (p.size() != 2) {
        ERS_LOG("Watched stream wrong format: must be type_name");
        continue;
      }
      Tag t0(p[0], p[1]);

      p.clear();
      boost::split(p, streams[1], boost::is_any_of("_"));
      if (p.size() != 2) {
        ERS_LOG("Watched stream wrong format: must be type_name");
        continue;
      }
      Tag t1(p[0], p[1]);

      m_secretParams.assignPolicyWatchedStreams[t0].insert(t1);
      m_secretParams.assignPolicyWatchedStreams[t1].insert(t0);
    }
    else {
      ERS_LOG("Unknown extra parameter " << parameter << ". Ignored.");
    }

    if (iss.fail()) {
      ERS_LOG("Malformed extra parameter " << parameter << ". Ignored.");
    }
  }
}

} // namespace SFOng

std::ostream & operator<<(std::ostream & os, SFOng::AssignPolicy const & p) {
  if (p == SFOng::AssignPolicy::RoundRobin) {
    os << "RoundRobin";
  }
  else if (p == SFOng::AssignPolicy::OccupancyWeighted) {
    os << "OccupancyWeighted";
  }
  else if (p == SFOng::AssignPolicy::DebugAssignErrors) {
    os << "DebugAssignErrors";
  }
  else {
    os << '?';
  }

  return os;
}
