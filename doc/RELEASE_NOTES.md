# SFOng

- added: periodic update of free buffer counter (IS) even when no data are received
  was: "0" free buffers was published when no data received giving the wrong
  impression that SFOng was about to assert backpressure.

## tdaq-09-04-00

- added: timeout waiting for DCM session close
- added: support for configuration with one writing path only
- replaced deprecated tbb::tbb_hasher with std::hash
- replaced deprecated tbb::atomic with std::atomic
- fixed: starting a new run without going to shutdown (stop/start) could produce Late events
  at the beginning of the new run for a transition period

## tdaq-08-03-01

### File Closing Strategy

File closing is now based on duration rather than on lumiblocks. Hence changing lumiblocks duration does not impact the lifetime of past lumiblocks' files. This was primarily 
implemented to better support ATLAS lumiscans where lumiblocks duration change from 1 min to
a few seconds for several minutes. 

Configuration Parameter Impacts:

- MaxOpenLumiblocks: removed
- MaxLumiblockLifetime: added

User Commands Impacts:

- maxOpenLumiblock: removed
- maxLumiblockLifetime {lifetime in seconds}: added
  Change the lifetime of files to the given value at runtime
- lbCloseMethodFallback {max open lumiblocks: added
  Revert the closing policy to the legacy method using provided value as MaxOpenLumiblocks

### Database-Synchronized Directory Unlocking

Directory unlocking can now be configured to synchronize with SFO-T0
database publication.

Configuration Paramter Impact:

- UnlockDirDbSync: added, default: true

SFOng locks writing directories with a SFO.lock file. To avoid performance degradation
due to read/write concurrency data readers can check this file to avoid reading while SFOng
is writing. The release of the lock on a specific directory can now be done once all files
written to this directory are published to the SFO-T0 database.

This was implemented to avoid rare cases where CastorScript was accessing the files after
the lock was released but before the files were published by the database leading to runtime
errors.  

## tdaq-07-01-00

### Stream to Thread Assignement Policy

Added configuration paramaters:

- AssignPolicy
- StreamWeightPeriod (default: 5 seconds)
- IOServiceWeightPeriod (default: 5 seconds)
  
Assignement policy of streams to thread is now configurable in two modes:

- RoundRobin: (*legacy mode*) new streams are assigned to the next thread arranged in a cycle
- Occupancy: SFOng computes loads associated to streams and updates the weight of each thread
  upon each assignment. Each stream is associated to the least loaded thread.
  
  The load is the amount of data processed on each stream in a sliding window representing
  the immediate past. Two extra config parameters exist to change the behavior of the 
  Occupancy policy: StreamWeightPeriod and IOServiceWeightPeriod. These are the sizes of the
  sliding windows used to compute load for, respectively, a stream and a thread.
